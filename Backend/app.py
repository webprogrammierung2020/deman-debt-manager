import os
import json
from flask import Flask, Response
from endpoints.usersEndpoints import users_app
from endpoints.transactionsEndpoints import transactions_app
from endpoints.notificationsEndpoints import notifications_app
from endpoints.groupsEndpoints import groups_app
from database.db_config import Config
from database.database import DB
from flask_migrate import Migrate

# some setup for the app
app = Flask(__name__)
app.config.from_object(Config)
DB.init_app(app)
migrate = Migrate(app, DB)

# register blueprints of endpoints for better structure
app.register_blueprint(users_app, url_prefix='/users')
app.register_blueprint(transactions_app, url_prefix='/transactions')
app.register_blueprint(notifications_app, url_prefix='/notifications')
app.register_blueprint(groups_app, url_prefix='/groups')

# enable CORS
@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,POST,OPTIONS')
    return response

# only for local testing
if __name__ == '__main__':
    app.run(debug = True)