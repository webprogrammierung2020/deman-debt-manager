"""fixed head

Revision ID: 79fe4f05d06f
Revises: d6f027ee1225
Create Date: 2021-03-22 17:00:02.210449

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '79fe4f05d06f'
down_revision = 'd6f027ee1225'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('transactionHeaders', sa.Column('subgroupID', sa.Integer(), nullable=True))
    op.drop_constraint('transactionHeaders_ibfk_3', 'transactionHeaders', type_='foreignkey')
    op.create_foreign_key(None, 'transactionHeaders', 'subgroups', ['subgroupID'], ['subgroupID'])
    op.drop_column('transactionHeaders', 'groupID')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('transactionHeaders', sa.Column('groupID', mysql.INTEGER(), autoincrement=False, nullable=True))
    op.drop_constraint(None, 'transactionHeaders', type_='foreignkey')
    op.create_foreign_key('transactionHeaders_ibfk_3', 'transactionHeaders', 'groups', ['groupID'], ['groupID'])
    op.drop_column('transactionHeaders', 'subgroupID')
    # ### end Alembic commands ###
