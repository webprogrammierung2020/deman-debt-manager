"""add fixed share

Revision ID: ba3b6b503ec4
Revises: 994636a4fd12
Create Date: 2021-03-26 09:38:29.083509

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ba3b6b503ec4'
down_revision = '994636a4fd12'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('transactionTails', sa.Column('share', sa.Numeric(precision=11, scale=2), nullable=False))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('transactionTails', 'share')
    # ### end Alembic commands ###
