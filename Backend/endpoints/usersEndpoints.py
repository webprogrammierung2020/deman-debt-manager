import os
import json
from decimal import *
from flask import Flask, Blueprint, request
from sqlalchemy import or_, and_
from database.db_models import User, TransactionHead, TransactionTail
from database.database import DB
from database.tokenValidator import check_token, validateUser

# Exceptions:
from sqlalchemy.orm.exc import UnmappedInstanceError
from werkzeug.exceptions import BadRequest

users_app = Blueprint('users_app', __name__)

@users_app.route('/<string:fbUID>', methods=['GET'])
@check_token
def getUserByFirebaseUID(fbUID):
    
    response = ""


    try:
        # check if the token belongs to the url userId
        if not validateUser(fbUID ,request.user["uid"]):
            response = {
                "statusCode": 401,
                "description": "The user from the url doesn't match the user found by the token",
                "headers": {
                    "Content-Type": "application/json",
                    "WWW-Authenticate": "user doesn't match the token owner" 
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 401
            return response
        
        user = User.query.filter_by(firebaseUID = fbUID).first()    # possible Exception ->  AttributeError
        
        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
                "user": json.loads(str(user))
            }))
        }

    except AttributeError as e:
        print(e)
        print(type(e))
        
        response = {
            "statusCode": 404,
            "description": "Couldn't find a user with that firebaseUID.",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
            }))
        }, 404
    except Exception as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 400,
            "description": "Invalid firebaseUID supplied",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
            }))
        }, 400
    
    return response

@users_app.route('/<string:fbUID>/balance', methods=['GET'])
@check_token
def getBalancesByFirebaseUID(fbUID):
    # Optional parameters:
    category = request.args.get('category')
    role = request.args.get('role')
    showOld = request.args.get('showOld')
    showProvisional = request.args.get('showProvisional')

    response = ""

    try:
        # check if the token belongs to the url userId
        if not validateUser(fbUID, request.user["uid"]):
            response = {
                "statusCode": 401,
                "description": "The user from the url doesn't match the user found by the token",
                "headers": {
                    "Content-Type": "application/json",
                    "WWW-Authenticate": "user doesn't match the token owner" 
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 401
            return response
        
        ### check the parameters ###
        if category not in {None, "plain", "bucket", "group"}:
            response = {
                "statusCode": 400,
                "description": "Invalid argument in optional key 'category'. Use 'plain', 'bucket' or 'group'",
                "headers": {
                    "Content-Type": "application/json"
                },
                "body": json.loads(json.dumps({
                    "path": request.path
                }))
            }, 400
            return response
        if role not in {None, "creditor", "debtor"}:
            response = {
                "statusCode": 400,
                "description": "Invalid argument in optional key 'role'. Use 'creditor' or 'debtor'",
                "headers": {
                    "Content-Type": "application/json"
                },
                "body": json.loads(json.dumps({
                    "path": request.path
                }))
            }, 400
            return response
        if showOld not in {None, "false", "true"}:
            response = {
                "statusCode": 400,
                "description": "Invalid argument in optional key 'showOld'. Use 'true' or 'false'",
                "headers": {
                    "Content-Type": "application/json"
                },
                "body": json.loads(json.dumps({
                    "path": request.path
                }))
            }, 400
            return response
        if showProvisional not in {None, "false", "true"}:
            response = {
                "statusCode": 400,
                "description": "Invalid argument in optional key 'showProvisional'. Use 'true' or 'false'",
                "headers": {
                    "Content-Type": "application/json"
                },
                "body": json.loads(json.dumps({
                    "path": request.path
                }))
            }, 400
            return response
        
        def getBalanceWithFilter(fbUID, role, deprecated, category, isValid):
            transactions = transactionsFilter(
                user = fbUID,
                role = role,
                deprecated = deprecated,
                category = category,
                isValid = isValid
            )

            balance = Decimal(0)

            for transaction in transactions:
                if role == "debtor" or role is None:
                    for tail in transaction.tails:
                        if tail.debtor == fbUID and tail.deprecated == False:
                            balance -= tail.share
                if role == "creditor" or role is None:
                    if transaction.creditor == fbUID:
                        balance += transaction.amount
            return balance
        
        balances = {}

        if role is None:
            totalBalance = getBalanceWithFilter(
                fbUID,
                None,
                None if showOld == "true" else False,
                category,
                None if showProvisional == "true" else True
            )
            balances.update({
                "total_balance": str(totalBalance)
            })
        if role is None or role == "creditor":
            receivablesBalance = getBalanceWithFilter(
                fbUID,
                "creditor",
                None if showOld == "true" else False,
                category,
                None if showProvisional == "true" else True
            )
            balances.update({
                "receivables_balance": str(receivablesBalance)
            })
        if role is None or role == "debtor":
            payablesBalance = 0 - getBalanceWithFilter(
                fbUID,
                "debtor",
                None if showOld == "true" else False,
                category,
                None if showProvisional == "true" else True
            )
            balances.update({
                "payables_balance": str(payablesBalance)
            })
        
                

        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps(balances))
        }
    except Exception as e:
        print(e)
        print(type(e))
        
        response = {
            "statusCode": 400,
            "description": "Invalid input",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
            }))
        }, 400

    return response

@users_app.route('/<string:fbUID>/transaction-hist', methods=['GET'])
@check_token
def getTransactionHistByFirebaseUID(fbUID):
    # Optional parameters:
    category = request.args.get('category')
    role = request.args.get('role')
    showOld = request.args.get('showOld')
    showProvisional = request.args.get('showProvisional')

    response = ""

    try:
        # check if the token belongs to the url userId
        if not validateUser(fbUID, request.user["uid"]):
            response = {
                "statusCode": 401,
                "description": "The user from the url doesn't match the user found by the token",
                "headers": {
                    "Content-Type": "application/json",
                    "WWW-Authenticate": "user doesn't match the token owner" 
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 401
            return response
        
        ### check the parameters ###
        if category not in {None, "plain", "bucket", "group"}:
            response = {
                "statusCode": 400,
                "description": "Invalid argument in optional key 'category'. Use 'plain', 'bucket' or 'group'",
                "headers": {
                    "Content-Type": "application/json"
                },
                "body": json.loads(json.dumps({
                    "path": request.path
                }))
            }, 400
            return response
        if role not in {None, "creditor", "debtor"}:
            response = {
                "statusCode": 400,
                "description": "Invalid argument in optional key 'role'. Use 'creditor' or 'debtor'",
                "headers": {
                    "Content-Type": "application/json"
                },
                "body": json.loads(json.dumps({
                    "path": request.path
                }))
            }, 400
            return response
        if showOld not in {None, "false", "true"}:
            response = {
                "statusCode": 400,
                "description": "Invalid argument in optional key 'showOld'. Use 'true' or 'false'",
                "headers": {
                    "Content-Type": "application/json"
                },
                "body": json.loads(json.dumps({
                    "path": request.path
                }))
            }, 400
            return response
        if showProvisional not in {None, "false", "true"}:
            response = {
                "statusCode": 400,
                "description": "Invalid argument in optional key 'showProvisional'. Use 'true' or 'false'",
                "headers": {
                    "Content-Type": "application/json"
                },
                "body": json.loads(json.dumps({
                    "path": request.path
                }))
            }, 400
            return response
        
        
        transactions = transactionsFilter(
            user = fbUID,
            role = role,
            deprecated = None if showOld == "true" else False,
            category = category,
            isValid = None if showProvisional == "true" else True
        )

        transactionStr = []
        for transaction in transactions:
            transactionStr.append(json.loads(str(transaction)))

        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
                "transaction_hist": transactionStr
            }))
        }
    except Exception as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 400,
            "description": "Invalid firebaseUID supplied",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 400

    return response

@users_app.route('/<string:fbUID>/balanceWith', methods=["GET"])
@check_token
def getBalanceWithUserByFirebaseUID(fbUID):
    otherfbUID = request.args.get('otherUser')

    response = ""

    try:
        # check if the token belongs to the url userId
        if not validateUser(fbUID ,request.user["uid"]):
            response = {
                "statusCode": 401,
                "description": "The user from the url doesn't match the user found by the token",
                "headers": {
                    "Content-Type": "application/json",
                    "WWW-Authenticate": "user doesn't match the token owner" 
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 401
            return response
        
        balance = balanceWithAnotherUser(fbUID, otherfbUID)
        

        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
                "balance": str(balance)
            }))
        }

    except AttributeError as e:
        print(e)
        print(type(e))
        
        response = {
            "statusCode": 404,
            "description": "Couldn't find a user with that firebaseUID.",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
            }))
        }, 404
    except Exception as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 400,
            "description": "Invalid firebaseUID supplied",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
            }))
        }, 400
    
    return response

@users_app.route('/<string:fbUID>/groups', methods=['GET'])
def getGroupsByFirebaseUID(fbUID):

    response = ""

    try:
        # fill this Array with the groups
        group_list =["just", "a", "filler"]

        # TODO implementation for DB connection

        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
                "groups": group_list
            }))
        }
    except:
        # TODO differentiate between 400 and 404
        response = {
            "statusCode": 400,
            "description": "Invalid firebaseUID supplied",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 400

        response = {
            "statusCode": 404,
            "description": "Couldn't find a user with that firebaseUID.",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
            }))
        }, 404

    return response

@users_app.route('/', methods=['POST'])
@users_app.route('/create', methods=['POST'])
def addUser():

    response = ""

    try:
        data = request.get_json()
        user = User(data["username"],data["firebaseUID"])

        DB.session.add(user)
        DB.session.flush()
        DB.session.commit()

        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
                "user": json.loads(str(user))
            }))
        }
        
    except Exception as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 405,
            "description": "Invalid input",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
                
            }))
        }, 400

    return response

@users_app.route('/', methods=['PATCH'])
@users_app.route('/update', methods=['PATCH'])
@check_token
def updateUser():

    response = ""

    try:
        data = request.get_json()   # possible Exception ->  BadRequest

        # check if the token belongs to the url firebaseUID
        if not validateUser(data['firebaseUID'] ,request.user["uid"]):
            response = {
                "statusCode": 401,
                "description": "The user from the url doesn't match the user found by the token",
                "headers": {
                    "Content-Type": "application/json",
                    "WWW-Authenticate": "user doesn't match the token owner" 
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 401
            return response

        user = User.query.filter_by(firebaseUID = data['firebaseUID']).first()
        user.username = data['username']
        user.balance = data['balance']
        DB.session.commit()

        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
                "user": json.loads(str(user))
            }))
        }
    except BadRequest as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 400,
            "description": "Bad request. The browser (or proxy) sent a request that this server could not understand.",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 400
    
    except Exception as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 404,
            "description": "Couldn't find a user with that firebaseUID.",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 404

    return response

@users_app.route('/', methods=['DELETE'])
@users_app.route('/delete', methods=['DELETE'])
@check_token
def deleteUser():

    response = ""

    
    try:
        fbUID = request.args['firebaseUID'] # possible Exception ->  werkzeug.exceptions.BadRequestKeyError

        # check if the token belongs to the url firebaseUID
        if not validateUser(fbUID ,request.user["uid"]):
            response = {
                "statusCode": 401,
                "description": "The user from the url doesn't match the user found by the token",
                "headers": {
                    "Content-Type": "application/json",
                    "WWW-Authenticate": "user doesn't match the token owner" 
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 401
            return response

        user = User.query.get(fbUID)
        DB.session.delete(user) # possible Exception ->  sqlalchemy.orm.exc.UnmappedInstanceError
        DB.session.commit()

        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
                "firebaseUID": fbUID
            }))
        }
    except UnmappedInstanceError as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 404,
            "description": "Couldn't find a user with that firebaseUID.",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 404
    
    except Exception as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 400,
            "description": "Invalid firebaseUID supplied",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 400

    return response

def transactionsFilter(**kwargs):
    queries = []
    
    # transactions where someone is creditor (always)
    if kwargs.get('user') is not None and kwargs.get('otherUser') is not None:
        if kwargs.get('role') is None:
            queries.append(
                or_(
                    and_(
                        TransactionHead.creditor == kwargs.get('user'),
                        TransactionTail.debtor == kwargs.get('otherUser')
                    ),
                    and_(
                        TransactionHead.creditor == kwargs.get('otherUser'),
                        TransactionTail.debtor == kwargs.get('user')
                    )
                )
            )
        elif kwargs.get('role') == "creditor":
            queries.append(
                and_(
                    TransactionHead.creditor == kwargs.get('user'),
                    TransactionTail.debtor == kwargs.get('otherUser')
                )
            )
        elif kwargs.get('role') == "debtor":
            queries.append(
                and_(
                    TransactionHead.creditor == kwargs.get('otherUser'),
                    TransactionTail.debtor == kwargs.get('user')
                )
            )
        else:
            raise KeyError('Invalid argument in "role"')
    elif kwargs.get('user') is not None and kwargs.get('otherUser') is None:
        if kwargs.get('role') is None:
            queries.append(
                or_(
                    TransactionHead.creditor == kwargs.get('user'),
                    TransactionTail.debtor == kwargs.get('user')
                )
            )
        elif kwargs.get('role') == 'creditor':
            queries.append(TransactionHead.creditor == kwargs.get('user'))
        elif kwargs.get('role') == 'debtor':
            queries.append(TransactionTail.debtor == kwargs.get('user'))
        else:
            raise KeyError('Invalid argument in "role"')
    
    for key, value in kwargs.items():
        if key == "deprecated" and value == True:
            queries.append(TransactionHead.deprecated == True)
        elif key == "deprecated" and value == False:
            queries.append(TransactionHead.deprecated == False)
        if key == "category" and value is not None:
            queries.append(TransactionHead.transactionType == value)
        if key == "isValid" and value == True:
            queries.append(TransactionHead.isValid == True)
        elif key == "isValid" and value == False:
            queries.append(TransactionHead.isValid == False)

    
    q = DB.session.query(TransactionHead).join(TransactionTail).filter(*queries)
    return q

def balanceWithAnotherUser(fbUID, otherfbUID):

    transactionsAsCreditor = transactionsFilter(
        user = fbUID,
        otherUser = otherfbUID,
        role = "creditor",
        isValid = True,
        deprecated = False
    ).all()

    transactionsAsDebtor = transactionsFilter(
        user = fbUID,
        otherUser = otherfbUID,
        role = "debtor",
        isValid = True,
        deprecated = False
    ).all()

    balance = Decimal(0)

    for transaction in transactionsAsCreditor:
        for tail in transaction.tails:
            if tail.debtor == otherfbUID and tail.deprecated == False:
                balance += tail.share

    for transaction in transactionsAsDebtor:
        for tail in transaction.tails:
            if tail.debtor == fbUID and tail.deprecated == False:
                balance -= tail.share
    return balance # balance as receivables (Forderungen)