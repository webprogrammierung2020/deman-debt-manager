import os
import json
from flask import Flask, Blueprint, request
from database.db_models import User, Group, Subgroup, TransactionHead
from database.database import DB
from database.tokenValidator import check_token, validateUser

groups_app = Blueprint('groups_app', __name__)

@groups_app.route('/', methods=['GET'])
@check_token
def getGroup():
    # ID parameter:
    groupID = request.args['groupID']

    response = ""

    try:
        g = Group.query.get(groupID)

        if g is None:
            raise KeyError

        for user in g.users:
            if validateUser(user.firebaseUID, request.user["uid"]):
                response = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json"
                    },
                    "body": json.loads(str(g))
                }
                return response
        
        response = {
                "statusCode": 401,
                "description": "No participant of the group matches the user found by the token",
                "headers": {
                    "Content-Type": "application/json",
                    "WWW-Authenticate": "participants of group doesn't match the token owner" 
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 401
    except KeyError as e:
        response = {
            "statusCode": 404,
            "description": "Couldn't find a group with that groupID",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 404

    except Exception as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 405,
            "description": "Invalid Input. Maybe you have used the wrong HTTP-method, then try PATCH or DELETE",
            "Allow": "POST | PATCH | DELETE",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 405

    return response

@groups_app.route('/', methods=['POST'])
@groups_app.route('/create', methods=['POST'])
@check_token
def addGroup():

    response = ""

    try:
        data = request.get_json()   # possible Exception ->  BadRequest

        group = Group(data["title"],data["description"],data["admin"])
        participants = data["participants"]
        for user in participants:
            group.users.append(User.query.filter_by(firebaseUID = user).first())

        DB.session.add(group)
        DB.session.flush()

        subgroup = Subgroup("Allgemein", group.groupID)
        DB.session.add(subgroup)
        DB.session.flush()
        group.subgroups.append(subgroup)

        DB.session.commit()


        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(str(group))
        }
    except Exception as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 405,
            "description": "Invalid Input. Maybe you have used the wrong HTTP-method, then try PATCH or DELETE",
            "Allow": "POST | PATCH | DELETE",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 405

    return response

@groups_app.route('/', methods=['PATCH'])
@groups_app.route('/update', methods=['PATCH'])
@check_token
def updateGroup():

    response = ""

    try:
        data = request.get_json()   # possible Exception ->  BadRequest
        group = Group.query.get(data["groupID"])    # possible Exeption -> KeyError | NotFound

        # check if the token belongs to the admin in this group
        if not validateUser(group.admin, request.user["uid"]):
            response = {
                "statusCode": 401,
                "description": "Admin of the group dont's matches the user found by the token",
                "headers": {
                    "Content-Type": "application/json",
                    "WWW-Authenticate": "admin of group doesn't match the token owner" 
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 401
            return response

        if 'title' in data:
            group.title = data["title"]
        if 'description' in data:
            group.description = data["description"]
        if 'admin' in data:
            group.admin = data["admin"]
        if 'join' in data:
            for user in data["join"]:
                try:
                    group.users.append(User.query.filter_by(firebaseUID = user).first())
                except Exception as e:
                    print(e)
                    print(type(e))
                    raise KeyError("the user {} has been not found.".format(user))
        if 'leave' in data:
            for user in data["leave"]:
                if user == group.admin:
                    raise AttributeError("The admin can't leave the group. You need to decide an other participant as admin.")
                try:
                    group.users.remove(User.query.filter_by(firebaseUID = user).first())
                except Exception as e:
                    print(e)
                    print(type(e))
                    raise KeyError("the user {} has been not found.".format(user))

        DB.session.commit()

        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(str(group))
        }

    except KeyError as e:
        response = {
            "statusCode": 404,
            "description": str(e),
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 404

    except AttributeError as e:
        response = {
            "statusCode": 400,
            "description": str(e),
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 404

    except Exception as e:
        print(e)
        print(type(e))
        # TODO differentiate between 404 and 405
        response = {
            "statusCode": 405,
            "description": "Invalid Input. Maybe you have used the wrong HTTP-method, then try POST or DELETE",
            "Allow": "POST | PATCH | DELETE",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 405

    return response

@groups_app.route('/<int:groupID>/leave', methods=['PATCH'])
@check_token
def leaveGroup(groupID):

    response = ""

    try:
        group = Group.query.get(groupID)

        if group is None:
            raise KeyError("no group with groupID {} founded".format(groupID))

        uid = request.user["uid"]
        
        if group.admin != uid:
            try:
                group.users.remove(User.query.filter_by(firebaseUID = uid).first())
            except Exception as e:
                print(e)
                print(type(e))
                raise KeyError("the user {} has been not found.".format(uid))
        else:
            raise AttributeError("The admin can't leave the group. You need to decide an other participant as admin")

        DB.session.commit()


        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(str(group))
        }

    except KeyError as e:
        response = {
            "statusCode": 404,
            "description": str(e),
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 404

    except AttributeError as e:
        response = {
            "statusCode": 400,
            "description": str(e),
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 400

    except Exception as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 405,
            "description": "Invalid Input. Maybe you have used the wrong HTTP-method, then try PATCH or DELETE",
            "Allow": "POST | PATCH | DELETE",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 405

    return response

@groups_app.route('/', methods=['DELETE'])
@groups_app.route('/delete/', methods=['DELETE'])
@check_token
def deleteGroup():
    # ID parameter:
    groupID = request.args['groupID']

    response = ""

    try:
        group = Group.query.get(groupID)

        if group is None:
            raise KeyError

        if not validateUser(group.admin, request.user["uid"]):
            response = {
                "statusCode": 401,
                "description": "Admin of the group dont's matches the user found by the token",
                "headers": {
                    "Content-Type": "application/json",
                    "WWW-Authenticate": "admin of group doesn't match the token owner" 
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 401
            return response

        for subgroup in group.subgroups:
            for transaction in subgroup.transactions:
                DB.session.delete(transaction)
            DB.session.delete(subgroup)
        DB.session.delete(group)
        DB.session.commit()

        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
                "message": groupID
            }))
        }

    except KeyError as e:
        response = {
            "statusCode": 404,
            "description": "Couldn't find a group with that groupId.",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 404

    except Exception as e:        
        print(e)
        print(type(e))

        response = {
            "statusCode": 400,
            "description": "Invalid groupId. The groupId must be an integer.",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 400

    return response

@groups_app.route('/<int:groupID>/subgroups', methods=['GET'])
@check_token
def getSubgroup(groupID):
    # ID parameter:
    subgroupID = request.args['subgroupID']

    response = ""

    try:
        s = Subgroup.query.get(subgroupID)

        if s is None:
            raise KeyError("Couldn't find a subgroup with that subgroupID in that groupID.")
        elif groupID != s.group.groupID:
            raise KeyError("This subgroup isn't in that group of your groupID.")

        for user in s.group.users:
            if validateUser(user.firebaseUID, request.user["uid"]):
                response = {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json"
                    },
                    "body": json.loads(str(s))
                }
                return response
        
        response = {
                "statusCode": 401,
                "description": "No participant of the group matches the user found by the token",
                "headers": {
                    "Content-Type": "application/json",
                    "WWW-Authenticate": "participants of group doesn't match the token owner" 
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 401
    except KeyError as e:
        response = {
            "statusCode": 404,
            "description": str(e),
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 404

    except Exception as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 405,
            "description": "Invalid Input. Maybe you have used the wrong HTTP-method, then try PATCH or DELETE",
            "Allow": "GET | POST | PATCH | DELETE",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 405

    return response

@groups_app.route('/<int:groupID>/subgroups', methods=['POST'])
@check_token
def addSubgroup(groupID):

    response = ""

    try:
        data = request.get_json()   # possible Exception ->  BadRequest

        group = Group.query.get(groupID)
        if group is None:
            raise KeyError("Couldn't find a group with that groupID.")

        subgroup = Subgroup(data["title"], groupID)
        group.subgroups.append(subgroup)
        DB.session.commit()

        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(str(subgroup))
        }

    except KeyError as e:
        response = {
            "statusCode": 404,
            "description": str(e),
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 404

    except Exception as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 405,
            "description": "Invalid Input. Maybe you have used the wrong HTTP-method, then try PATCH or DELETE",
            "Allow": "POST | PATCH | DELETE",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 405

    return response

@groups_app.route('/<int:groupID>/subgroups', methods=['PATCH'])
@check_token
def updateSubgroup(groupID):
    # ID parameter:
    subgroupID = request.args['subgroupID']
    
    response = ""

    try:
        data = request.get_json()   # possible Exception ->  BadRequest
        s = Subgroup.query.get(subgroupID)

        if s is None:
            raise KeyError("Couldn't find a subgroup with that subgroupID in that groupID.")
        elif groupID != s.group.groupID:
            raise KeyError("This subgroup isn't in that group of your groupID.")
       
        if not validateUser(s.group.admin, request.user["uid"]):
            response = {
                "statusCode": 401,
                "description": "Admin of the group dont's matches the user found by the token",
                "headers": {
                    "Content-Type": "application/json",
                    "WWW-Authenticate": "admin of group doesn't match the token owner" 
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 401
            return response
        
        if 'title' in data:
            s.title = data["title"]
        if 'switch' in data:
            for ele in data["switch"]:
                founded = False
                for subgroup in s.group.subgroups:
                    if ele["newSubgroupID"] == subgroup.subgroupID:
                        transaction = Transaction.query.get(ele["transactionID"])
                        if transaction is None:
                            raise KeyError("No transaction has been founded with this ID: {}".format(ele["transactionID"]))
                        transaction.subgroupID = ele["newSubgroupID"]
                        founded = True
                        break

                if not founded:
                    raise KeyError("the subgroupID({}) doesn't match with one subgroupID of the group.".format(ele["newSubgroupID"]))

        DB.session.commit()

        response = {
                "statusCode": 200,
                "headers": {
                    "Content-Type": "application/json"
                },
                "body": json.loads(str(s))
            }

    except KeyError as e:
        response = {
            "statusCode": 404,
            "description": str(e),
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 404

    except Exception as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 405,
            "description": "Invalid Input. Maybe you have used the wrong HTTP-method, then try PATCH or DELETE",
            "Allow": "GET | POST | PATCH | DELETE",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 405

    return response

@groups_app.route('/<int:groupID>/subgroups', methods=['DELETE'])
@check_token
def deleteSubgroup(groupID):
    # ID parameter:
    subgroupID = request.args['subgroupID']
    
    response = ""

    try:
        s = Subgroup.query.get(subgroupID)

        if s is None:
            raise KeyError("Couldn't find a subgroup with that subgroupID in that groupID.")
        elif groupID != s.group.groupID:
            raise KeyError("This subgroup isn't in that group of your groupID.")

        if not validateUser(s.group.admin, request.user["uid"]):
            response = {
                "statusCode": 401,
                "description": "Admin of the group dont's matches the user found by the token",
                "headers": {
                    "Content-Type": "application/json",
                    "WWW-Authenticate": "admin of group doesn't match the token owner" 
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 401
            return response
        
        for transaction in s.transactions:
            DB.session.delete(transaction)
        DB.session.delete(s)
        DB.session.commit()

        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": {
                "delete ID": s.subgroupID
            }
        }

    except KeyError as e:
        response = {
            "statusCode": 404,
            "description": str(e),
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 404

    except Exception as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 405,
            "description": "Invalid Input. Maybe you have used the wrong HTTP-method, then try PATCH or DELETE",
            "Allow": "GET | POST | PATCH | DELETE",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 405

    return response