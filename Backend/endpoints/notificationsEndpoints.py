import os
import json
from flask import Flask, Blueprint, request
from database.db_models import Notification, TransactionHead, TransactionTail, Subgroup, Group
from endpoints.transactionsEndpoints import performCompensation
from database.database import DB
from database.tokenValidator import check_token, validateUser

# Exceptions:
from sqlalchemy.orm.exc import UnmappedInstanceError
from werkzeug.exceptions import BadRequest

notifications_app = Blueprint('notifications_app', __name__)

@notifications_app.route('/<string:fbUID>', methods=['GET'])
@check_token
def findNotificationsForUserById(fbUID):
    # Optional parameters:
    notifId = request.args.get('notificationId')
    showOld = request.args.get('showRead')

    response = ""

    try:
        # check if the token belongs to the url userId
        if not validateUser(fbUID, request.user["uid"]):
            response = {
                "statusCode": 401,
                "description": "The user from the url doesn't match the user found by the token",
                "headers": {
                    "Content-Type": "application/json",
                    "WWW-Authenticate": "user doesn't match the token owner" 
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 401
            return response
        
        ### define query by the parameters ###
        if notifId is None:
            if showOld == "true":
                notifs = Notification.query.filter_by(recipient = fbUID)   # possible Exception ->  AttributeError
            else:
                notifs = Notification.query.filter_by(
                    recipient = fbUID, isRead = False)   # possible Exception ->  AttributeError
        else:
            # a notifID got provided in the query so only the specific notif is requested
            notifs = Notification.query.filter_by(
                recipient = fbUID, notificationID = notifId)   # possible Exception ->  AttributeError
        
        notif_list = []
        for notif in notifs:
            notif_list.append(json.loads(str(notif)))

        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": {
                "path": request.path,
                "notification_list": json.loads(json.dumps(notif_list))
            }
        }
    except Exception as e:
        print(e)
        print(type(e))
        
        response = {
            "statusCode": 404,
            "description": "Couldn't find a notif with that parameters.",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 404

    return response

@notifications_app.route('/<int:notifId>/mark-as-read', methods=['PATCH'])
@check_token
def markNotificationAsRead(notifId):

    response = ""

    try:
        answer = request.args['answer'] # possible Exception ->  werkzeug.exceptions.BadRequestKeyError
        notif = Notification.query.filter_by(notificationID = notifId).first()   # possible Exception ->  AttributeError

        # check if the token belongs to the notif recipient
        if not validateUser(notif.recipient, request.user["uid"]):
            response = {
                "statusCode": 401,
                "description": "The notification recipient doesn't match the user found by the token",
                "headers": {
                    "Content-Type": "application/json",
                    "WWW-Authenticate": "recipient of the notification doesn't match the token owner" 
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 401
            return response

        if notif.isRead == True:
            response = {
                "statusCode": 409,
                "description": "The request is in conflict with the current state of the server."+
                    " The requested notification is already marked as read",
                "headers": {
                    "Content-Type": "application/json",
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 409
            return response
        
        content = json.loads(notif.content)

        # differentiate the usecases and execute them
        if content['purpose'] == "create-PlainTransaction":

            head = TransactionHead.query.filter_by(transactionID = content['transactionID']).first()

            if answer == "agree":
                if head.createdBy == head.creditor:
                    head.tails[0].acknowledged = True
                else:
                    head.acknowledged = True
                head.isValid = True
            elif answer == "disagree":
                DB.session.delete(head.tails[0])
                DB.session.delete(head)
            else:
                raise KeyError('Invalid argument in "answer"')
            
            notif.answer = answer

            ### create new Notif for feedback ###
            newContent = json.dumps({
                "purpose": "response-on-creation-of-PlainTransaction",
                "transactionID": head.transactionID,
                "expectedAnswers": "'ok'",
                "answer": answer,
                "share": str(head.amount),
                "transactionDescription": head.description
            })

            newNotif = Notification(notif.recipient, head.createdBy, newContent)
            DB.session.add(newNotif)
        elif content['purpose'] == "create-BucketTransaction":
            
            head = TransactionHead.query.filter_by(transactionID = content['transactionID']).first()
            
            if head is None:
                DB.session.delete(notif)
                DB.session.commit()

                response = {
                    "statusCode": 404,
                    "description": "The transaction referenced by the notification wasn't found, "+
                        "because another participant declined the transaction whereby it got removed. "+
                        "The notification got deleted since there is no further use.",
                    "headers": {
                        "Content-Type": "application/json"
                    },
                    "body": json.loads(json.dumps({
                        "path": request.path
                    }))
                }, 404
                return response

            if answer == "agree":

                if notif.recipient == head.creditor:
                    head.acknowledged = True

                transactionSuccess = True if head.acknowledged == True else False
                acknowledgedCount = 1 if head.acknowledged == True else 0
                
                for tail in head.tails:
                    if notif.recipient == tail.debtor:
                        tail.acknowledged = True
                    if tail.acknowledged == False:
                        transactionSuccess = False
                    else:
                        acknowledgedCount += 1
                
                if transactionSuccess:
                    head.isValid = True
                
            elif answer == "disagree":
                for tail in head.tails:
                    DB.session.delete(tail)
                DB.session.delete(head)
                acknowledgedCount = 0
            else:
                raise KeyError('Invalid argument in "answer"')
            
            notif.answer = answer

            ### create new Notif for feedback ###
            newContent = json.dumps({
                "purpose": "response-on-creation-of-BucketTransaction",
                "transactionID": head.transactionID,
                "expectedAnswers": "'ok'",
                "answer": answer,
                "acknowledged_progress": \
                    str(acknowledgedCount) + "/" + str(len(head.tails)+1),
                "share": str(head.amount),
                "transactionDescription": head.description
            })

            newNotif = Notification(notif.recipient, head.createdBy, newContent)
            DB.session.add(newNotif)
        elif content['purpose'] == "create-GroupTransaction":
            
            head = TransactionHead.query.filter_by(transactionID = content['transactionID']).first()
            subgroup = Subgroup.query.filter_by(subgroupID = head.subgroupID).first()
            group = subgroup.group

            if answer == "agree":
                head.isValid = True
            elif answer == "disagree":
                for tail in head.tails:
                    DB.session.delete(tail)
                DB.session.delete(head)
            else:
                raise KeyError('Invalid argument in "answer"')

            notif.answer = answer

            ### create new Notifs for feedback ###
            for user in group.users:
                if user.firebaseUID == group.admin:
                    continue
                newContent = json.dumps({
                    "purpose": "response-on-creation-of-GroupTransaction",
                    "transactionID": head.transactionID,
                    "expectedAnswers": "'ok'",
                    "answer": answer,
                    "share": str(head.amount),
                    "transactionDescription": head.description
                })
                newNotif = Notification(
                    # sender
                    group.admin,
                    # recipient
                    user.firebaseUID,
                    # content
                    newContent
                )
                DB.session.add(newNotif)
        elif content['purpose'] == "create-Compensation":
            
            head = TransactionHead.query.filter_by(transactionID = content['transactionID']).first()

            if answer == "agree":
                if head.createdBy == head.creditor:
                    head.tails[0].acknowledged = True
                else:
                    head.acknowledged = True
                head.isValid = True
                try:
                    performCompensation(head)
                except Exception() as e:
                    DB.session.delete(head.tails[0])
                    DB.session.delete(head)
                    response = {
                        "statusCode": 409,
                        "description": "The request conflicts with the current state of the server."+ 
                                "The Compensation is not valid any more due to a lower balance",
                        "headers": {
                            "Content-Type": "application/json"
                        },
                        "body": json.loads(json.dumps({
                            "path": request.path
                        }))
                    }, 409
                    return response
            elif answer == "disagree":
                DB.session.delete(head.tails[0])
                DB.session.delete(head)
            else:
                raise KeyError('Invalid argument in "answer"')
            
            notif.answer = answer

            ### create new Notif for feedback ###
            newContent = json.dumps({
                "purpose": "response-on-creation-of-Compensation",
                "transactionID": head.transactionID,
                "expectedAnswers": "'ok'",
                "answer": answer,
                "share": str(head.amount),
            })

            newNotif = Notification(notif.recipient, head.createdBy, newContent)
            DB.session.add(newNotif)
        elif content['purpose'] == "response-on-creation-of-PlainTransaction":
            # No action required, just note down the answer and mark as read
            
            if answer == "ok":
                notif.answer = answer
            else:
                raise KeyError('Invalid argument in "answer"')
        elif content['purpose'] == "response-on-creation-of-BucketTransaction":
            # No action required, just note down the answer and mark as read
            
            if answer == "ok":
                notif.answer = answer
            else:
                raise KeyError('Invalid argument in "answer"')
        elif content['purpose'] == "response-on-creation-of-GroupTransaction":

            if answer == "ok":
                notif.answer = answer
            else:
                raise KeyError('Invalid argument in "answer"')
        elif content['purpose'] == "response-on-creation-of-Compensation":

            if answer == "ok":
                notif.answer = answer
            else:
                raise KeyError('Invalid argument in "answer"')
        # TODO more usecases ##########################

        notif.isRead = True
        DB.session.flush()
        DB.session.commit()

        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
                "notification": json.loads(str(notif))
            }))
        }
    except AttributeError as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 404,
            "description": "Couldn't find a notification with that notificationId.",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 404
    
    except KeyError as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 400,
            "description": "Invalid argument in 'answer'. Use one of the expected answers given by the notification",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 400

    except Exception as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 400,
            "description": "Invalid Input.",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 400

    return response

@notifications_app.route('/', methods=['DELETE'])
@notifications_app.route('/delete', methods=['DELETE'])
@check_token
def deleteNotification():
    
    response = ""

    try:
        notifId = request.args['notificationId'] # possible Exception ->  werkzeug.exceptions.BadRequestKeyError

        notif = Notification.query.get(notifId)

        # check if the token belongs to the notif recipient
        if not validateUser(notif.recipient ,request.user["uid"]):
            response = {
                "statusCode": 401,
                "description": "The notification recipient doesn't match the user found by the token",
                "headers": {
                    "Content-Type": "application/json",
                    "WWW-Authenticate": "notification recipient doesn't match the token owner" 
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 401
            return response

        DB.session.delete(notif)    # possible Exception ->  sqlalchemy.orm.exc.UnmappedInstanceError
        DB.session.commit()

        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
                "notificationId": request.args['notificationId']
            }))
        }
    except AttributeError as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 404,
            "description": "Couldn't find a notification with that notificationId.",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 404
    
    except Exception as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 400,
            "description": "Invalid notificationId supplied",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 400

    return response