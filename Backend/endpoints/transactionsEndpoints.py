import os
import json
from flask import Flask, Blueprint, request
from decimal import Decimal
from datetime import datetime
from database.db_models import Notification, TransactionHead, TransactionTail, Group, Subgroup
from endpoints.usersEndpoints import balanceWithAnotherUser
from database.database import DB
from database.tokenValidator import check_token, validateUser

transactions_app = Blueprint('transactions_app', __name__)

@transactions_app.route('/<int:transactionId>', methods=['GET'])
@check_token
def findTransactionById(transactionId):

    response = ""

    try:
        transactionHead = TransactionHead.query.filter_by(transactionID = transactionId).first()   # possible Exception ->  AttributeError
        
        # check if the token belongs to the url userId
        validationSuccess = False
        for tail in transactionHead.tails:
            if validateUser(tail.debtor, request.user['uid']):
                validationSuccess = True
        if not validateUser(transactionHead.creditor, request.user["uid"]) and not validationSuccess:
            response = {
                "statusCode": 401,
                "description": "No participant of the transaction matches the user found by the token",
                "headers": {
                    "Content-Type": "application/json",
                    "WWW-Authenticate": "participants of the transactions doesn't match the token owner" 
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 401
            return response

        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": {
                "transaction": json.loads(str(transactionHead))
            }
        }
    except Exception as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 404,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 404

    return response

####################### PLAIN TRANSACTIONS #############################
@transactions_app.route('/plain', methods=['POST'])
@transactions_app.route('/plain/add', methods=['POST'])
@check_token
def addPlaintransaction():

    response = ""

    try:
        data = request.get_json()

        # check if the token belongs to the notif recipient
        if not validateUser(data['creditor'], request.user["uid"])\
                and not validateUser(data['participants'][0]['debtor'], request.user["uid"]):
            response = {
                "statusCode": 401,
                "description": "No participant of the transactions matches the user found by the token",
                "headers": {
                    "Content-Type": "application/json",
                    "WWW-Authenticate": "participants of transaction doesn't match the token owner" 
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 401
            return response
        
        ### code for developing without proper token validation #########################################################################
        requestUser = "14ZKbFpAtVTpulvUkeGPecCVtfc2" if request.user["uid"] == "admin" else request.user["uid"]

        ### create Transaction ###
        transactionHead = TransactionHead(
            ### code for developing without proper token validation #########################################################################
            requestUser, data['creditor'], Decimal(data['amount']), None, "plain", data.get('description')
        )

        DB.session.add(transactionHead)
        DB.session.flush() # to get the id of the transactionHead

        tail = TransactionTail(
            data['participants'][0]['debtor'], Decimal(data['amount']), transactionHead.transactionID
        )

        DB.session.add(tail)
        DB.session.flush()

        
        if requestUser == data['creditor']:
            transactionRecipient = data['participants'][0]['debtor']
            transactionHead.acknowledged = True
        else:
            transactionRecipient = data['creditor']
            tail.acknowledged = True
        DB.session.commit()

        ### create Notification ###
        content = json.dumps({
            "purpose": "create-PlainTransaction",
            "expectedAnswers": "'agree' | 'disagree'",
            "share": str(transactionHead.amount),
            "transactionDescription": transactionHead.description,
            "transactionID": transactionHead.transactionID
        })

        notif = Notification(requestUser ,transactionRecipient, content)

        DB.session.add(notif)
        DB.session.flush()
        DB.session.commit()


        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
                "notification": json.loads(str(notif)),
                "transaction": json.loads(str(transactionHead))
            }))
        }
    except Exception as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 400,
            "description": "Invalid Input.",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 400

    return response

@transactions_app.route('/plain', methods=['PATCH'])
def updatePlaintransaction():

    response = ""

    try:
        # TODO implementation for DB connection

        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
                "transactionId": transactionId,
                "createdBy": "",
                "createdAt": "2021-03-12T16:01:07.242Z",
                "creditorUID": "",
                "sum": "",
                "debitorUID": "",
                "acknowledged": "",
            }))
        }
    except:
        # TODO differentiate between 404 and 405
        response = {
            "statusCode": 405,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 405

    return response

####################### BUCKET TRANSACTIONS ############################
@transactions_app.route('/bucket', methods=['POST'])
@transactions_app.route('/bucket/add', methods=['POST'])
@check_token
def addBuckettransation():

    response = ""

    try:
        data = request.get_json()

        # check if the token belongs to the notif recipient
        validationSuccess = False
        for participant in data['participants']:
            if validateUser(participant.get('debtor'), request.user['uid']):
                validationSuccess = True
        if not validateUser(data['creditor'], request.user["uid"]) and not validationSuccess:
            response = {
                "statusCode": 401,
                "description": "No participant of the transactions matches the user found by the token",
                "headers": {
                    "Content-Type": "application/json",
                    "WWW-Authenticate": "participants of transaction doesn't match the token owner" 
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 401
            return response
        
        ### code for developing without proper token validation #########################################################################
        requestUser = "14ZKbFpAtVTpulvUkeGPecCVtfc2" if request.user["uid"] == "admin" else request.user["uid"]

        ### create Transaction ###
        transactionHead = TransactionHead(
            ### code for developing without proper token validation #########################################################################
            requestUser, data['creditor'], Decimal(data['amount']), None, "bucket", data.get('description')
        )

        if requestUser == data['creditor']:
            transactionHead.acknowledged = True

        DB.session.add(transactionHead)
        DB.session.flush() # to get the id of the transactionHead


        sumOfShare = Decimal(0)
        for participant in data['participants']:
            sumOfShare += Decimal(participant['share'])

            tail = TransactionTail(
                participant['debtor'], participant['share'], transactionHead.transactionID
            )
            if requestUser == participant['debtor']:
                tail.acknowledged = True
            
            DB.session.add(tail)
        DB.session.flush()
        
        if sumOfShare != Decimal(transactionHead.amount):
            response = {
                "statusCode": 400,
                "description": "Invalid Input. The sum of all shares must be equal to the amount. " +
                    "If you try splitting the amount even, adjust the amount so that it is aliquot.",
                "headers": {
                    "Content-Type": "application/json"
                },
                "body": json.loads(json.dumps({
                    "path": request.path
                }))
            }, 400
            return response
        DB.session.commit()

        ### create Notification ###
        notifications = []
        for tail in transactionHead.tails:
            content = {
                "purpose": "create-BucketTransaction",
                "expectedAnswers": "'agree' | 'disagree'",
                "share": str(tail.share),
                "transactionDescription": transactionHead.description,
                "transactionID": transactionHead.transactionID
            }
            
            if requestUser == tail.debtor:
                # this is because a debtor could create the transaction
                # and if thats the case not he but the crceditor gets the notif
                transactionRecipient = transactionHead.creditor
                content.update({
                    "share": str(transactionHead.amount)
                })
            else:
                transactionRecipient = tail.debtor
            
            notif = Notification(requestUser, transactionRecipient, json.dumps(content))
            notifications.append(notif)
            DB.session.add(notif)
            DB.session.flush()

        DB.session.commit()
        
        notif_list = list(map(lambda x: json.loads(str(x)), notifications))

        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
                "notifications": json.loads(json.dumps(notif_list)),
                "transaction": json.loads(str(transactionHead))
            }))
        }
    except Exception as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 400,
            "description": "Invalid Input.",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 400

    return response

@transactions_app.route('/bucket', methods=['PATCH'])
def updateBuckettransaction():

    response = ""

    try:
        # TODO implementation for DB connection

        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
                "transactionId": transactionId,
                "createdBy": "",
                "createdAt": "2021-03-12T16:01:07.242Z",
                "creditorUID": "",
                "sum": "",
                "debitorUID": "",
                "acknowledged": "",
            }))
        }
    except:
        # TODO differentiate between 404 and 405
        response = {
            "statusCode": 405,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 405

    return response

####################### GROUP TRANSACTIONS #############################
@transactions_app.route('/group', methods=['POST'])
@check_token
def addGrouptransation():

    response = ""

    try:
        data = request.get_json()
        subgroup = Subgroup.query.get(data["subgroupID"])

        # check if the token belongs to the group
        validationSuccess = False
        for u in subgroup.group.users:
            if validateUser(u.firebaseUID, request.user['uid']):
                validationSuccess = True
        if not validationSuccess:
            response = {
                "statusCode": 401,
                "description": "No participant of the group matches the user found by the token",
                "headers": {
                    "Content-Type": "application/json",
                    "WWW-Authenticate": "participants of group doesn't match the token owner" 
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 401
            return response

        ### code for developing without proper token validation #########################################################################
        requestUser = "nhIJ7x1xLjS6g6nyH5FpGmMXWxx1" if request.user["uid"] == "admin" else request.user["uid"]
        
        ### create Transaction ###
        tHead = TransactionHead(
            requestUser, data["creditor"], data['amount'], data["subgroupID"], "group"
        )

        if requestUser == data["creditor"]:
            tHead.acknowledged = True

        DB.session.add(tHead)
        DB.session.flush()

        for user in subgroup.group.users:
            if user.firebaseUID == tHead.creditor:
                continue
            tail = TransactionTail(
                user.firebaseUID,
                tHead.amount / len(subgroup.group.users),
                tHead.transactionID
            )
            tail.acknowledged = True
            DB.session.add(tail)
        DB.session.flush()

        ### create Notification ###
        notif = Notification(
            # sender
            requestUser,
            # recipient
            subgroup.group.admin,
            # content
            json.dumps({
                "purpose": "create-GroupTransaction",
                "expectedAnswers": "'agree' | 'disagree'",
                "amount": tHead.amount,
                "transactionID": tHead.transactionID
            })
        )
        DB.session.add(notif)
        DB.session.commit()

        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "notification": json.loads(str(notif)),
                "transaction": json.loads(str(tHead))
            }))
        }
    except Exception as e:
        print(e)
        print(type(e))
        response = {
            "statusCode": 405,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 405

    return response

@transactions_app.route('/group', methods=['PATCH'])
def updateGroupTransaction():

    response = ""

    try:
        # TODO implementation for DB connection

        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
                "transactionId": transactionId,
                "createdBy": "",
                "createdAt": "2021-03-12T16:01:07.242Z",
                "creditorUID": "",
                "sum": "",
                "debitorUID": "",
                "acknowledged": "",
            }))
        }
    except:
        # TODO differentiate between 404 and 405
        response = {
            "statusCode": 405,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 405

    return response


####################### COMPENSATIONS ##################################
@transactions_app.route('/compensation', methods=['POST'])
@check_token
def addCompensation():
    
    response = ""

    try:
        data = request.get_json()

        # check if the token belongs to the notif recipient
        if not validateUser(data['creditor'], request.user["uid"])\
                and not validateUser(data['participants'][0]['debtor'], request.user["uid"]):
            response = {
                "statusCode": 401,
                "description": "No participant of the compensation matches the user found by the token",
                "headers": {
                    "Content-Type": "application/json",
                    "WWW-Authenticate": "participants of compensation doesn't match the token owner" 
                },
                "body": json.loads(json.dumps({
                    "path": request.path,
                }))
            }, 401
            return response
        
        ### code for developing without proper token validation #########################################################################
        requestUser = "14ZKbFpAtVTpulvUkeGPecCVtfc2" if request.user["uid"] == "admin" else request.user["uid"]

        compensationAmount = Decimal(data['amount'])
        balance = balanceWithAnotherUser(data['creditor'], data['participants'][0]['debtor'])
        
        if compensationAmount > 0:
            raise Exception("amount for compensation must be negative")
        ### create Transaction ###
        transactionHead = TransactionHead(
            ### code for developing without proper token validation #########################################################################
            requestUser, data['creditor'], compensationAmount, None, "compensation"
        )

        DB.session.add(transactionHead)
        DB.session.flush()

        tail = TransactionTail(
            data['participants'][0]['debtor'], compensationAmount, transactionHead.transactionID
        )

        DB.session.add(tail)
        DB.session.flush()

        if requestUser == data['creditor']:
            transactionRecipient = data['participants'][0]['debtor']
            transactionHead.acknowledged = True
        else:
            transactionRecipient = data['creditor']
            tail.acknowledged = True
        
        ### create Notification ###
        content = {
            "purpose": "create-Compensation",
            "expectedAnswers": "'agree' | 'disagree'",
            "share": str(transactionHead.amount),
            "transactionDescription": transactionHead.description,
            "transactionID": transactionHead.transactionID
        }

        if balance < Decimal(compensationAmount):
            transferInfo = data.get('transferInfo')
            if transferInfo is None:
                raise Exception("The compensation amount is more than the currently open balance. "+
                    "If you intended to do a transfer then please use the 'transferInfo' argument in the body.")
            content.update({
                "transferInfo": transferInfo
            })
        
        notif = Notification(requestUser, transactionRecipient, json.dumps(content))

        DB.session.add(notif)
        DB.session.flush()
        DB.session.commit()


        response = {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path,
                "notification": json.loads(str(notif)),
                "transaction": json.loads(str(transactionHead))
            }))
        }
    
    except Exception as e:
        print(e)
        print(type(e))

        response = {
            "statusCode": 400,
            "description": "Invalid Input.",
            "headers": {
                "Content-Type": "application/json"
            },
            "body": json.loads(json.dumps({
                "path": request.path
            }))
        }, 400
    
    return response


def performCompensation(transactionHead):
    creditor = transactionHead.creditor # C
    debtor = transactionHead.tails[0].debtor # D
    compensationAmount = Decimal(transactionHead.amount) 
    balance = balanceWithAnotherUser(creditor, debtor)
    saldo = balance

    if saldo < 0:
        raise AttributeError('compensationAmount {} is greater that the balance {} between {} as creditor and {} as debtor').format(compensationAmount, balance, creditor, debtor)
    
    transactAsCreditor = DB.session.query(TransactionHead).\
        filter(TransactionHead.creditor == creditor).\
        filter(TransactionTail.debtor == debtor).\
        filter(TransactionHead.isValid == True).\
        filter(TransactionHead.deprecated == False).all()
    transactAsDebtor = DB.session.query(TransactionHead).\
        filter(TransactionHead.creditor == debtor).\
        filter(TransactionTail.debtor == creditor).\
        filter(TransactionHead.isValid == True).\
        filter(TransactionHead.deprecated == False).all()
    
    # deprecate all transaction.tails where C -> D
    for transaction in transactAsCreditor:
        for tail in transaction.tails:
            if tail.debtor == debtor and tail.deprecated == False:
                # update tail
                tail.deprecated = True

                # update head
                transaction.outstandingBalance -= tail.share 
                if transaction.outstandingBalance <= 0:
                    transaction.deprecated = True
    
    # deprecate all transaction.tails where D -> C
    for transaction in transactAsDebtor:
        for tail in transaction.tails:
            if tail.debtor == creditor and tail.deprecated == False:
                # update tail
                tail.deprecated = True

                # update head
                transaction.outstandingBalance -= tail.share 
                if transaction.outstandingBalance <= 0:
                    transaction.deprecated = True
    
    if saldo == 0:
        pass
    elif saldo > 0:
        ### create Transaction and set deprecated = True (saldo)
        thead = TransactionHead(transactionHead.createdBy, debtor, saldo, None, "saldo")
        thead.outstandingBalance = 0
        thead.description = "Saldo in Höhe von {}".format(saldo)
        thead.acknowledged = True
        thead.isValid = True
        thead.deprecated = True
        DB.session.add(thead)
        DB.session.flush()
        # create Tail
        ttail = TransactionTail(creditor, saldo, thead.transactionID)
        ttail.acknowledged = True
        ttail.deprecated = True
        DB.session.add(ttail)
        DB.session.flush()

        ### create Transaction for the new history
        thead = TransactionHead(transactionHead.createdBy, creditor, saldo, None, "saldo")
        thead.description = "Restposten aus letzter Ausgleichszahlung in Höhe von {}".format(saldo)
        thead.acknowledged = True
        thead.isValid = True
        DB.session.add(thead)
        DB.session.flush()
        # create Tail
        ttail = TransactionTail(debtor, saldo, thead.transactionID)
        ttail.acknowledged = True
        DB.session.add(ttail)
        DB.session.flush()

    DB.session.commit()
    return thead