# Readme for the Backend

The Backend is implemented with AWS Lambda / API Gateway - for a serverless implementation -, Flask - a Python package for Backend development - and Zappy to deploy the app in the AWS environment. \
Because of that make sure that you have Python 3.8 installed. Then install the Flask and Zappa package in a virtual environment. To do that follow these steps:

## Setup

### Install Python and the virtual enviroment
1. Download and install Python 3.8 from [the official Website](https://www.python.org/downloads/) (Notice that currently only Python3.8 is supported by Zappa and Lambda so be sure to use that particular version)

2. Navigate to the project folder `./Backend` and create the virtual environment, by opening the console and running the following command: \
`$ python -m venv venv`

3. Activate the virtual invironment with the command: \
`$ venv\Scripts\activate`
    
    The commandline should now start with `(venv) $ _` \
    Make sure that this is the case in any further steps, otherwise activate it again.

### Install required dependencies

4. Install the required dependencies with the following command: \
`(venv) $ pip install -r requirements.txt`

    ..or install the following dependencies manually: \
    `flask`, `zappa`, `flask-sqlalchemy`, `pymysql`, `firebase-admin`, `flask-migrate`

### Set the AWS Permission credentials
5. Navigate to your local home directory \
`C:\> cd %UserProfile%`
6. Navigate into the `.aws` directory. \
Edit the `credentials` file as follows and modify the `****` portions to include the User's keys given by the Admin:
    ```
    [default]
    aws_access_key_id = ****
    aws_secret_access_key = ****
    [zappa]
    aws_access_key_id = ****
    aws_secret_access_key = ****
    ```
    Then edit the `config` file like this:
    ```
    [default]
    region = eu-central-1
    output = json

    [zappa]
    region = eu-central-1
    output = json
    ```
    (If there is no `.aws` directory you should create one with both files)

The Setup should be finished now.

## Update the app with Zappa
To update the modified Code of your app just use the following command: \
`(venv) $ zappa update dev`

## Migrate the Database
### Setup
1. First you need to init the migration directory with the cmd command: \
`(venv) $ flask db init`

2. Then you need to develop your project as far as the `db_config.py` is finished and `db_models.py` are exist with the first table and columns

### Create the local migration
3. Now you can create your first migration version: \
`(venv) $ flask db migrate -m "Name of the version"`

You find the list of your database versions in the directory `./Backend/migrations/versions`. 

### The nomenclature from SQL-Alchemy
The nomenclature from SQL-Alchemy uses the `snake case`.
E.g. if you name your model class `User` the table will be named `user`.
When you named your model class like `AdressAndPhone`, the name of table in the database is `adress_and_phone`. If you prefer to choose your own table names, you can add an attribute named `__tablename__` to the model class, set to the desired name as a string.

### Upgrade your database 
4. If you use a MySQL or PostgreSQL database make sure that the database is already exist and you can connect to this. If you use a SQLite database the following command always delete the existing database and will create a new one.
To upload/upgrade your migration version into the database you simply use the command \
`(venv) $ flask db upgrade`

### Downgrade your database
5. To downgrade your database use the following command: \
`(venv) $ flask db downgrade`

## Tips while developing
While under development you don't have to update the AWS Lambda Funktion with Zappa to test the API. You can also run the code locally with Flask by navigating running the `app.py` file in your virtual enviroment. \
`(venv) $\Backend> python app.py`

Also a useful approach while developing with visual studio code is to install the extension `apexsql` and connect to the database so you can view the tables etc.
