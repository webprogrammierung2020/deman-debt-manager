from database.database import DB
from datetime import datetime
import json

groupmapper = DB.Table('groupmapper',
    DB.Column('groupID', DB.Integer, DB.ForeignKey('groups.groupID'), primary_key=True, nullable=False),
    DB.Column('firebaseUID', DB.String(50), DB.ForeignKey('users.firebaseUID'), primary_key=True, nullable=False)
)

class User(DB.Model):
    __tablename__ = 'users'
    firebaseUID = DB.Column(DB.String(50), primary_key=True, nullable=False)
    username = DB.Column(DB.String(30), nullable=False)
    transactionsAsCreditor = DB.relationship('TransactionHead',
        foreign_keys="TransactionHead.creditor", backref='user', lazy=False)
    transactionsAsDebtor = DB.relationship('TransactionTail',
        foreign_keys="TransactionTail.debtor", backref='user', lazy=False)
    groups = DB.relationship('Group', secondary=groupmapper, lazy='subquery',
        backref=DB.backref('users', lazy=True))
    
    def __repr__(self):
        return '<User {}>'.format(self.username)
    
    def __init__(self, username, firebaseUID):
        self.username = username
        self.firebaseUID = firebaseUID
    
    def __str__(self):
        return json.dumps({
            "username": self.username,
            "firebaseUID": self.firebaseUID
        })

class Group(DB.Model):
    __tablename__= 'groups'
    groupID = DB.Column(DB.Integer, primary_key=True, nullable=False)
    title = DB.Column(DB.String(50), nullable=False)
    description = DB.Column(DB.String(1023))
    admin = DB.Column(DB.String(50), DB.ForeignKey('users.firebaseUID'),
        nullable=False)
    subgroups = DB.relationship('Subgroup', backref='group', lazy=False)
    
    def __init__(self, title, description, admin):
        self.title = title
        self.description = description
        self.admin = admin
    
    def __repr__(self):
        return '<Group {}, {}>'.format(self.title, self.admin)

    def __str__(self):
        s = {
            "groupID": self.groupID,
            "title": self.title,
            "description": self.description,
            "admin": self.admin
            }
        
        p = []
        for user in self.users:
            p.append({
                "username": user.username,
                "firebaseUID": user.firebaseUID
            })
        s.update({
            "participants": p
        })

        subgroups = []
        for subgroup in self.subgroups:
            subgroups.append({
                "title": subgroup.title,
                "subgroupID": subgroup.subgroupID
            })
        s.update({
            "subgroups": subgroups
        })
        
        return json.dumps(s)

class Subgroup(DB.Model):
    __tablename__= 'subgroups'
    subgroupID = DB.Column(DB.Integer, primary_key=True, nullable=False)
    title = DB.Column(DB.String(255), nullable=False)
    groupID = DB.Column(DB.Integer, DB.ForeignKey('groups.groupID'))
    transactions = DB.relationship('TransactionHead', backref='subgroup', lazy=False)

    def __init__(self, title, groupID):
        self.title = title
        self.groupID = groupID
    
    def __repr__(self):
        return '<Subgroup {}>'.format(self.title)
    
    def __str__(self):
        return json.dumps({
            "subgroupID": self.subgroupID,
            "groupID": self.groupID,
            "title": self.title,
            })

class TransactionHead(DB.Model):
    __tablename__= 'transactionHeaders'
    transactionID = DB.Column(DB.Integer, primary_key=True, nullable=False)
    transactionType = DB.Column(DB.String(20), nullable=False)
    createdBy = DB.Column(DB.String(50), DB.ForeignKey('users.firebaseUID'),
        nullable=False)
    createdAt = DB.Column(DB.DateTime, nullable=False)
    creditor = DB.Column(DB.String(50), DB.ForeignKey('users.firebaseUID'),
        nullable=False)
    amount = DB.Column(DB.Numeric(11,2), nullable=False)
    outstandingBalance = DB.Column(DB.Numeric(11,2), nullable=False)
    description = DB.Column(DB.String(1023))
    acknowledged = DB.Column(DB.Boolean, nullable=False)
    isValid = DB.Column(DB.Boolean, nullable=False)
    deprecated = DB.Column(DB.Boolean, nullable=False)
    subgroupID = DB.Column(DB.Integer, DB.ForeignKey('subgroups.subgroupID'))
    tails = DB.relationship('TransactionTail', backref='head', lazy=False)

    def __init__(self, createdBy, creditor, amount, subgroupID, transactionType, description):
        self.createdBy = createdBy
        self.createdAt = '{:%Y-%m-%d %H:%M:%S}'.format(datetime.utcnow())
        self.creditor = creditor
        self.amount = amount
        self.acknowledged = False
        self.subgroupID = subgroupID
        self.isValid = False
        self.outstandingBalance = amount
        self.deprecated = False
        self.transactionType = transactionType
        self.description = description
    
    def __repr__(self):
        return '<Transaction {}>'.format(self.transactionID)
    
    def __str__(self):

        s = {
                "transactionID": self.transactionID,
                "createdAt": '{:%Y-%m-%dT%H:%M:%S}.{:.3}Z'\
                        .format(self.createdAt, str(self.createdAt.microsecond)),
                "createdBy": self.createdBy,
                "creditor": self.creditor,
                "amount": str(self.amount),
                "acknowledgedByCreditor": self.acknowledged,
                "isValid": self.isValid,
                "outstandingBalance": str(self.outstandingBalance),
                "deprecated": self.deprecated,
                "transactionType": self.transactionType,
                "description": self.description
            }
        
        if self.subgroupID is not None:
            s.update({"subgroupID": self.subgroupID})

        p = []
        for tail in self.tails:
            p.append({
                "debtor": tail.debtor,
                "acknowledgedByDebtor": tail.acknowledged,
                "share": str(tail.share),
                "deprecated": tail.deprecated
            })
        s.update({"participants": p})

        return json.dumps(s)
    
class TransactionTail(DB.Model):
    __tablename__= 'transactionTails'
    transactionTailID = DB.Column(DB.Integer, primary_key=True, nullable=False)
    debtor = DB.Column(DB.String(50), DB.ForeignKey('users.firebaseUID'),
        nullable=False)
    acknowledged = DB.Column(DB.Boolean)
    deprecated = DB.Column(DB.Boolean, nullable=False)
    share = DB.Column(DB.Numeric(11,2), nullable=False)
    headerID = DB.Column(DB.Integer, DB.ForeignKey(
        'transactionHeaders.transactionID'), nullable=False)
    
    def __init__(self, debtor, share, headerID):
        self.debtor = debtor
        self.acknowledged = False
        self.share = share
        self.headerID = headerID
        self.deprecated = False

class Notification(DB.Model):
    __tablename__ = 'notifications'
    notificationID = DB.Column(DB.Integer, primary_key=True, nullable=False)
    createdAt = DB.Column(DB.DateTime, nullable=False)
    sender = DB.Column(DB.String(50), DB.ForeignKey('users.firebaseUID'),
        nullable=False)
    recipient = DB.Column(DB.String(50), DB.ForeignKey('users.firebaseUID'),
        nullable=False)
    content = DB.Column(DB.String(1023), nullable=False)
    isRead = DB.Column(DB.Boolean)
    answer = DB.Column(DB.String(20))

    def __init__(self, sender, recipient, content):
        self.sender = sender
        self.recipient = recipient
        self.createdAt = '{:%Y-%m-%d %H:%M:%S}'.format(datetime.utcnow())
        self.content = content
        self.isRead = False
        self.answer = "no-answer"

    
    def __str__(self):
        return json.dumps({
            "notificationID": self.notificationID,
            "createdAt": '{:%Y-%m-%dT%H:%M:%S}.{:.3}Z'\
                    .format(self.createdAt, str(self.createdAt.microsecond)),
            "recipient": self.recipient,
            "content": json.loads(self.content),
            "isRead": self.isRead,
            "answer": self.answer,
            "sender": self.sender
        })