# import pyrebase
import firebase_admin
import json
from functools import wraps
from flask import request
from firebase_admin import credentials, auth

# Connect to firebase
cred = credentials.Certificate('fbAdminConfig.json')
firebase = firebase_admin.initialize_app(cred)
# pb = pyrebase.initialize_app(json.load(open('fbconfig.json')))

# token validation
def check_token(f):
    """Verifies a Firebase token from the "authorization" field in the header. \n
    Returns Statuscode 400 if the token was invalid or no token was given at all.
    Returns the function if the token could be located to a user.
    That User gets written in the request.user object.
    """

    @wraps(f)
    def wrap(*args,**kwargs):
        if not request.headers.get('authorization'):
            ### temporary code for developing ### remove bevore going live ###
            request.user = {"uid": "admin"}
            return f(*args, **kwargs)
            ### end of developing code #######################################
            return {'message': 'No token provided'},400
        try:
            user = auth.verify_id_token(request.headers['authorization'])
            request.user = user
        except Exception as e:
            print(e)
            return {'message':'Invalid token provided.'},400
        return f(*args, **kwargs)
    return wrap


def validateUser(urlUser, tokenUser):
    """Simple function to check if two Users are the same.

    Args:
        urlUser (string): fbUID from the URL
        tokenUser (string): fbUID from the token validation

    Returns:
        bool: true if the User is the requested one. False if not.
    """
    if tokenUser == "admin":
        return True

    if urlUser == tokenUser:
        return True
    else:
        return False