from flask_sqlalchemy import SQLAlchemy

# this only exists to declare the db object for dependencies
# so no circular dependencies will occur
DB = SQLAlchemy()