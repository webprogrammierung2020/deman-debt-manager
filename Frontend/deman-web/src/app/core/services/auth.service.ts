import { Injectable } from '@angular/core';
import { BehaviorSubject, from, Observable, of } from 'rxjs';
import firebase from 'firebase';
import {
  selectAuthError,
  selectIsLoadingAuth,
  selectIsLoggedIn,
} from '@core/store/auth/auth.selectors';
import { Actions, createEffect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { AuthActions } from '@core/store/auth/auth-types';
import { catchError, filter, map, switchMap, take, tap } from 'rxjs/operators';
import { AuthInfos } from '@shared/interfaces/auth-informations';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { CoreState } from '@core/store';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { User, UserResponse, UserService } from '@build/openapi';
import { notEmpty } from '@shared/utils/not-empty';
import { UserActions } from '@core/store/user/user-types';
import { SnackbarService } from '@core/services/snackbar.service';
import { MatSnackBarDismiss, MatSnackBarRef, TextOnlySnackBar } from '@angular/material/snack-bar';
import { EmailVerificationHintComponent } from '@shared/components/email-verification-hint/email-verification-hint.component';
import UserCredential = firebase.auth.UserCredential;
import { NotificationEntityService } from '@core/store/ngrx-data/notifications/notification-entity.service';
import { TransactionEntityService } from '@core/store/ngrx-data/transactions/transaction-entity.service';

@Injectable()
export class AuthService {
  // ************************************************
  // Observable Queries available for consumption by views
  // ************************************************

  firebaseUser$: BehaviorSubject<firebase.User | undefined> = new BehaviorSubject<
    firebase.User | undefined
  >(undefined);
  latestEmailSnackRef$: BehaviorSubject<
    MatSnackBarRef<EmailVerificationHintComponent | TextOnlySnackBar> | undefined
  > = new BehaviorSubject<
    MatSnackBarRef<EmailVerificationHintComponent | TextOnlySnackBar> | undefined
  >(undefined);
  emailVerified$: Observable<boolean> = this.firebaseUser$.pipe(
    filter(notEmpty),
    map((firebaseUser: firebase.User) => firebaseUser.emailVerified)
  );
  loading$: Observable<boolean> = this.store.select(selectIsLoadingAuth);
  error$: Observable<string | null> = this.store.select(selectAuthError);
  isLoggedIn$: Observable<boolean> = this.store.select(selectIsLoggedIn);

  // ************************************************
  // Effects to be registered at the Module level
  // ************************************************

  getUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ROOT_EFFECTS_INIT),
      switchMap(() => {
        return this.angularFireAuth.authState.pipe(
          map((firebaseUser: firebase.User | null) => {
            if (firebaseUser) {
              console.log(firebaseUser);
              this.firebaseUser$.next(firebaseUser);
              return AuthActions.loginSuccess({
                firebaseUid: firebaseUser.uid,
                email: firebaseUser.email!,
              });
            } else {
              return AuthActions.Unauthorized();
            }
          }),
          catchError((error) =>
            of(AuthActions.loginError(error.message || 'Problems reaching firebase backend!'))
          )
        );
      })
    );
  });

  handleLoginSuccess$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.loginSuccess),
      map((action) => {
        return UserActions.getDeManUser({ firebaseUid: action.firebaseUid, email: action.email });
      })
    );
  });

  signUp$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AuthActions.signUpRequest),
        map((action) => action.authInfos),
        switchMap((authInfos: AuthInfos) => {
          // this will update the af Auth State
          return this.signUpToFirebase(authInfos.email!, authInfos.password!).pipe(
            tap((userCredentials: firebase.auth.UserCredential) =>
              userCredentials.user?.sendEmailVerification()
            ),
            catchError((error) => {
              this.store.dispatch(AuthActions.loginError({ errorMessage: error.message }));
              return of(null);
            })
          );
        })
      );
    },
    { dispatch: false }
  );

  login$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AuthActions.loginRequest),
        switchMap((action) => {
          return this.signInToFirebase(action.authInfos).pipe(
            catchError((error) => {
              this.store.dispatch(AuthActions.loginError({ errorMessage: error.message }));
              return of(null);
            })
          );
        })
      );
    },
    { dispatch: false }
  );

  logout$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AuthActions.logout),
        tap(() => {
          this.angularFireAuth.signOut().then(() => {
            this.latestEmailSnackRef$.getValue()?.dismiss();
            this.store.dispatch(AuthActions.closeEmailVerifiedSnack());

            this.latestEmailSnackRef$.next(undefined);
            this.firebaseUser$.next(undefined);

            this.notificationEntityService.clearCache();
            this.transactionEntityService.clearCache();

            this.router.navigateByUrl('');
          });
        })
      );
    },
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private angularFireAuth: AngularFireAuth,
    private angularFireStore: AngularFirestore,
    private store: Store<CoreState>,
    private router: Router,
    private userService: UserService,
    private snackBarService: SnackbarService,
    private notificationEntityService: NotificationEntityService,
    private transactionEntityService: TransactionEntityService
  ) {}

  // ******************************************
  // Static
  // ******************************************

  private static setPersistence(rememberMe: boolean): Promise<void> {
    return firebase
      .auth()
      .setPersistence(
        rememberMe ? firebase.auth.Auth.Persistence.LOCAL : firebase.auth.Auth.Persistence.SESSION
      );
  }

  // ******************************************
  // Service Methods
  // ******************************************

  public signUp(authInfos: AuthInfos): void {
    AuthService.setPersistence(authInfos.rememberMe)
      .then(() => {
        this.store.dispatch(AuthActions.signUpRequest({ authInfos }));
      })
      .catch((error) =>
        this.store.dispatch(AuthActions.loginError({ errorMessage: error.message }))
      );
  }

  public logout(): void {
    this.angularFireAuth.signOut().then(() => this.store.dispatch(AuthActions.logout()));
  }

  public login(authInfos: AuthInfos): void {
    AuthService.setPersistence(authInfos.rememberMe)
      .then(() => this.store.dispatch(AuthActions.loginRequest({ authInfos })))
      .catch((error) =>
        this.store.dispatch(AuthActions.loginError({ errorMessage: error.message }))
      );
  }

  public resetPassword(email: string): Promise<void> {
    return this.angularFireAuth.sendPasswordResetEmail(email);
  }

  public sendEmailVerification(): Promise<void> {
    const currentUser = this.firebaseUser$.getValue();
    return new Promise((resolve, reject) => {
      if (currentUser !== undefined) {
        return resolve(currentUser.sendEmailVerification());
      } else {
        return reject('User is undefined!');
      }
    });
  }

  public handleEmailVerificationHint(): void {
    this.createUserHint(this.firebaseUser$.getValue());
  }

  // ******************************************
  // Internal Methods
  // ******************************************

  private signUpToFirebase(
    email: string,
    password: string
  ): Observable<firebase.auth.UserCredential> {
    return from(this.angularFireAuth.createUserWithEmailAndPassword(email, password));
  }

  private signInToFirebase(authInf: AuthInfos): Observable<UserCredential> {
    return from(this.angularFireAuth.signInWithEmailAndPassword(authInf.email, authInf.password));
  }

  private createUser(user: User): Observable<User> {
    return this.userService
      .addUser(user)
      .pipe(map((userResponse: UserResponse) => userResponse.body.user));
  }

  private createUserHint(firebaseUser: firebase.User | undefined): void {
    if (firebaseUser === undefined) {
      // wait 2s and retry
      setTimeout(() => this.handleEmailVerificationHint(), 2000);
      return;
    }
    if (!firebaseUser.emailVerified) {
      const snackBarRef = this.snackBarService.showEmailVerifiedHint();
      this.registerSnack(snackBarRef);
      snackBarRef
        .afterDismissed()
        .pipe(take(1))
        .subscribe(() => {
          this.sendEmailVerification()
            .then(() => {
              const successSnackBarRef = this.snackBarService.openDefaultSnack(
                'Verifizierungs-Email wurde gesendet!',
                'Nachricht Verbergen',
                { duration: 2000 }
              );
              this.registerSnack(successSnackBarRef);
              successSnackBarRef
                .afterDismissed()
                .pipe(take(1))
                .subscribe(() => this.handleEmailVerificationHint());
            })
            .catch((error) => console.log(error)); // TODO show error
        });
    } else {
      this.latestEmailSnackRef$.getValue()?.dismiss();
      this.store.dispatch(AuthActions.closeEmailVerifiedSnack());
    }
  }

  private registerSnack(
    snackBarRef: MatSnackBarRef<EmailVerificationHintComponent | TextOnlySnackBar>
  ): void {
    this.store.dispatch(AuthActions.openEmailVerifiedSnack());
    this.latestEmailSnackRef$.next(snackBarRef);
  }
}
