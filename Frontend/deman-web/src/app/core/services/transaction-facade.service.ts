import { Injectable } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { TransactionEntityService } from '@core/store/ngrx-data/transactions/transaction-entity.service';
import { Observable } from 'rxjs';
import { Transaction, UserService } from '@build/openapi';
import { filter, first, map, switchMap, take, withLatestFrom } from 'rxjs/operators';
import { toPlainTransaction } from '@shared/utils/to-plain-transaction';
import { toBucketTransaction } from '@shared/utils/to-bucket-transaction';
import { AuthService } from '@core/services/auth.service';
import { toDebts } from '@shared/utils/to-debts';
import { notEmpty } from '@shared/utils/not-empty';
import firebase from 'firebase';
import { Store } from '@ngrx/store';
import { CoreState } from '@core/store';
import {
  selectErrorBucket,
  selectErrorDebt,
  selectErrorPlain,
  selectLoadedBucketTransactions,
  selectLoadedPlainTransactions,
} from '@core/store/transaction/transaction.selectors';
import { TransactionActions } from '@core/store/transaction/transaction-types';
import { toClaims } from '@shared/utils/to-claims';

@Injectable()
export class TransactionFacadeService {
  transactions$: Observable<Transaction[]> = this.transactionEntityService.entities$;

  claims$: Observable<Transaction[]> = this.authService.firebaseUser$.pipe(
    take(1),
    filter(notEmpty),
    withLatestFrom(this.transactionEntityService.entities$),
    map(toClaims)
  );

  plainTransactions$: Observable<Transaction[]> = this.transactionEntityService.entities$.pipe(
    map(toPlainTransaction)
  );

  bucketTransactions$: Observable<Transaction[]> = this.transactionEntityService.entities$.pipe(
    map(toBucketTransaction)
  );

  debts$: Observable<Transaction[]> = this.authService.firebaseUser$.pipe(
    take(1),
    filter(notEmpty),
    withLatestFrom(this.transactionEntityService.entities$),
    map(toDebts)
  );

  loading$: Observable<boolean> = this.transactionEntityService.loading$;

  loadedPlainClaims$: Observable<boolean> = this.store.select(selectLoadedPlainTransactions);
  loadedBucketClaims$: Observable<boolean> = this.store.select(selectLoadedBucketTransactions);
  loadedDebts$: Observable<boolean> = this.store.select(selectLoadedBucketTransactions);

  errorPlainClaims$: Observable<string> = this.store
    .select(selectErrorPlain)
    .pipe(filter(notEmpty));
  errorBucketClaims$: Observable<string> = this.store
    .select(selectErrorBucket)
    .pipe(filter(notEmpty));
  errorDebts$: Observable<string> = this.store.select(selectErrorDebt).pipe(filter(notEmpty));

  constructor(
    private action$: Actions,
    private transactionEntityService: TransactionEntityService,
    private authService: AuthService,
    private userService: UserService,
    private store: Store<CoreState>
  ) {}

  public getDebts(): Observable<Transaction[]> {
    let fbUser: firebase.User;
    return this.authService.firebaseUser$.pipe(
      filter(notEmpty),
      take(1),
      switchMap((firebaseUser: firebase.User) => {
        fbUser = firebaseUser;
        return this.userService
          .getTransactionHistByFirebaseUID(firebaseUser.uid, 'plain', 'debtor')
          .pipe(map((transactionResposne) => transactionResposne.body.transaction_hist));
      }),
      switchMap((plainResponse) => {
        return this.userService
          .getTransactionHistByFirebaseUID(fbUser.uid, 'bucket', 'debtor')
          .pipe(map((response) => [...plainResponse, ...response.body.transaction_hist]));
      }),
      first()
    );
  }

  public setLoadedPlain(): void {
    this.store.dispatch(TransactionActions.loadingPlainTransactionSucceed());
  }

  public setLoadedBucket(): void {
    this.store.dispatch(TransactionActions.loadingBucketTransactionSucceed());
  }

  public setLoadedDebts(): void {
    this.store.dispatch(TransactionActions.loadingDebtTransactionSucceed());
  }

  public resetCallStates(): void {
    this.store.dispatch(TransactionActions.resetForReload());
  }
}
