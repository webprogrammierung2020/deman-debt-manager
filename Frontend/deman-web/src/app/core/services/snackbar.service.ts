import { Injectable, Injector } from '@angular/core';
import {
  MatSnackBar,
  MatSnackBarConfig,
  MatSnackBarRef,
  TextOnlySnackBar,
} from '@angular/material/snack-bar';
import { EmailVerificationHintComponent } from '@shared/components/email-verification-hint/email-verification-hint.component';

@Injectable({
  providedIn: 'root',
})
export class SnackbarService {
  constructor(private injector: Injector, private snackBar: MatSnackBar) {}

  showEmailVerifiedHint(): MatSnackBarRef<EmailVerificationHintComponent> {
    return this.snackBar.openFromComponent(EmailVerificationHintComponent, {
      panelClass: 'pizza-party',
    });
  }

  openDefaultSnack(
    message: string,
    action: string,
    options?: MatSnackBarConfig
  ): MatSnackBarRef<TextOnlySnackBar> {
    return this.snackBar.open(message, action, options);
  }
}
