import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { NotificationEntityService } from '@core/store/ngrx-data/notifications/notification-entity.service';
import { Observable } from 'rxjs';
import { Notification, User } from '@build/openapi';
import { UserActions } from '@core/store/user/user-types';
import { filter, first, map, switchMap, take } from 'rxjs/operators';
import { AuthService } from '@core/services/auth.service';
import { notEmpty } from '@shared/utils/not-empty';

@Injectable()
export class NotificationService {
  notifications$: Observable<Notification[]> = this.notificationEntityService.entities$;
  loading$: Observable<boolean> = this.notificationEntityService.loading$;
  loaded$: Observable<boolean> = this.notificationEntityService.loaded$;

  getNotificationsOnGetDeManUserSuccess$ = createEffect(
    () => {
      return this.action$.pipe(
        ofType(UserActions.getDeManUserSuccess),
        map((action) => action.deManUser),
        switchMap((deManUser: User) => {
          return this.notificationEntityService.getWithQuery({
            firebaseUid: deManUser.firebaseUID,
          });
        })
      );
    },
    { dispatch: false }
  );

  constructor(
    private action$: Actions,
    private notificationEntityService: NotificationEntityService,
    private authService: AuthService
  ) {}

  public reloadNotifications(): Observable<Notification[]> {
    return this.authService.firebaseUser$.pipe(
      take(1),
      filter(notEmpty),
      switchMap((fbUser) => {
        return this.notificationEntityService.getWithQuery({ firebaseUid: fbUser.uid });
      }),
      first()
    );
  }
}
