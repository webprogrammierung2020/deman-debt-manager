import { Injectable } from '@angular/core';
import {
  NotificationPurpose,
  ReadableNotification,
} from '@shared/interfaces/readable-notification';
import { Notification, NotificationContent } from '@build/openapi';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ReadableNotificationFactory {
  constructor() {}

  private static createReadableNotificationForCreate(
    notification: NotificationContent
  ): ReadableNotification | undefined {
    switch (notification.purpose) {
      case 'create-PlainTransaction':
        return environment.plainTransactionNotificationBase.create;
      case 'create-BucketTransaction':
        return environment.bucketTransactionNotificationBase.create;
      case 'create-GroupTransaction':
        return environment.groupTransactionNotificationBase.create;
      case 'create-Compensation':
        return environment.compensationNotificationBase.create;
      default:
        return undefined;
    }
  }

  private static createReadableNotificationForResponse(
    notification: NotificationContent
  ): ReadableNotification | undefined {
    switch (notification.purpose) {
      case 'response-on-creation-of-BucketTransaction':
        return environment.bucketTransactionNotificationBase.response;
      case 'response-on-creation-of-Compensation':
        return environment.compensationNotificationBase.response;
      case 'response-on-creation-of-GroupTransaction':
        return environment.groupTransactionNotificationBase.response;
      case 'response-on-creation-of-PlainTransaction':
        return environment.plainTransactionNotificationBase.response;
      default:
        return undefined;
    }
  }

  create(notification: NotificationContent): ReadableNotification | undefined {
    if (notification.purpose === undefined) {
      return undefined;
    }
    if (notification.purpose!.split('-')[0] === NotificationPurpose.CREATE) {
      console.log('in create');
      return ReadableNotificationFactory.createReadableNotificationForCreate(notification);
    } else {
      return ReadableNotificationFactory.createReadableNotificationForResponse(notification);
    }
  }
}
