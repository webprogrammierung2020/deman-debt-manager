import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { CoreState } from '@core/store';
import { User, UserResponse, UserService } from '@build/openapi';
import { Observable, of } from 'rxjs';
import {
  selectDeManUser,
  selectDeManUserError,
  selectIsLoadingDeManUser,
} from '@core/store/user/user.selector';
import { notEmpty } from '@shared/utils/not-empty';
import { catchError, filter, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { UserActions } from '@core/store/user/user-types';
import { AuthActions } from '@core/store/auth/auth-types';
import { Router } from '@angular/router';
import { selectSignUpAlias } from '@core/store/auth/auth.selectors';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthService } from '@core/services/auth.service';

@Injectable()
export class DeManUserService {
  user$: Observable<User> = this.store.select(selectDeManUser).pipe(filter(notEmpty));
  loading$: Observable<boolean> = this.store.select(selectIsLoadingDeManUser);
  error$: Observable<string> = this.store.select(selectDeManUserError).pipe(filter(notEmpty));

  getDeManUserOnLogin$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.getDeManUser),
      withLatestFrom(this.store.select(selectSignUpAlias)),
      switchMap(([action, alias]) => {
        console.log('alias: ' + alias);
        return this.userService.getUserByFirebaseUID(action.firebaseUid).pipe(
          map((userResponse: UserResponse) => {
            const deManUser: User = userResponse.body.user;
            return UserActions.getDeManUserSuccess({ deManUser });
          }),
          catchError((error: HttpErrorResponse) => {
            console.log(alias);
            console.log(error.error.statusCode);
            console.log(error.error.description);
            if (error.error.statusCode === 400) {
              // user does not exist create a new one!
              return of(
                UserActions.registerDeManUser({
                  alias,
                  firebaseUid: action.firebaseUid,
                  email: action.email,
                })
              );
            } else {
              return of(
                UserActions.getDeManUserFailed({
                  errorMessage: error.error.description || 'Internal Server Error',
                })
              );
            }
          })
        );
      })
    );
  });

  registerNewUserOnSignUp$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.registerDeManUser),
      switchMap((action) => {
        return this.userService
          .addUser({ username: action.alias, firebaseUID: action.firebaseUid, email: action.email })
          .pipe(
            map((userResponse: UserResponse) =>
              UserActions.getDeManUserSuccess({ deManUser: userResponse.body.user })
            ),
            // TODO logout on error with message to user
            catchError((error: HttpErrorResponse) => of(AuthActions.logout()))
          );
      })
    );
  });

  navigateOnGetDeManUser$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(UserActions.getDeManUserSuccess),
        tap(() => {
          this.router.navigateByUrl('dashboard').then(() => {
            this.authService.handleEmailVerificationHint();
          });
        })
      );
    },
    { dispatch: false }
  );

  deleteDeManUserOnLogout$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.logout),
      map(() => {
        return UserActions.removeUser();
      })
    );
  });

  constructor(
    private actions$: Actions,
    private store: Store<CoreState>,
    private userService: UserService,
    private router: Router,
    private authService: AuthService
  ) {}
}
