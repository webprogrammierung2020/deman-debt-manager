import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { AuthService } from '@core/services/auth.service';
import { environment } from '../../../../../environments/environment';
import { MatSidenav } from '@angular/material/sidenav';
import { MatDialog } from '@angular/material/dialog';
import { NotificationListComponent } from '@shared/components/notification-list/notification-list.component';
import { NotificationService } from '@core/services/notification.service';
import { Notification } from '@build/openapi';
import { notEmpty } from '@shared/utils/not-empty';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@Component({
  selector: 'core-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.scss'],
})
export class SidenavListComponent implements OnInit {
  personalRoutes = environment.personalRoutes;
  groupRoutes = environment.groupRoutes;
  accountRoutesLoggedIn = environment.accountRoutesLoggedIn;
  accountRoutesNotLoggedIn = environment.accountRoutesNotLoggedIn;
  // notificationCount: number;
  @ViewChild('sidenav') sideNav: MatSidenav;

  isLoggedIn$: Observable<{ isLoggedIn: boolean }> = this.authService.isLoggedIn$.pipe(
    map((isLoggedIn: boolean) => ({ isLoggedIn }))
  );

  notificationCount$: Observable<number> = this.notificationService.notifications$.pipe(
    map((notifications: Notification[]) =>
      notifications.reduce((acc: number, val: Notification) => (val.isRead ? acc : acc + 1), 0)
    ),
    tap((count) => console.log(count))
  );

  constructor(
    private authService: AuthService,
    private dialog: MatDialog,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {}

  onLogout(): void {
    this.authService.logout();
    this.sideNav.close();
  }

  onOpenNotifications(): void {
    this.dialog.open(NotificationListComponent);
    this.sideNav.close();
  }
}
