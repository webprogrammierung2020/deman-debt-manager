import { Component, OnInit } from '@angular/core';
import { DeManUserService } from '@core/services/de-man-user.service';
import { User } from '@build/openapi';
import { Observable } from 'rxjs';

@Component({
  selector: 'core-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent {
  user$: Observable<User> = this.deManUserService.user$;
  constructor(private deManUserService: DeManUserService) {}
}
