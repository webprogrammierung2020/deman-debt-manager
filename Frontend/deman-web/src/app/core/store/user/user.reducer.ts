import { User } from '@build/openapi/model/user';
import { CallState, LoadingState } from '@shared/interfaces/call-state';
import { createReducer, on } from '@ngrx/store';
import { UserActions } from '@core/store/user/user-types';

export interface UserState {
  user: User | undefined;
  userCallState: CallState;
}

export const initialUserState: UserState = {
  user: undefined,
  userCallState: LoadingState.INIT,
};

export const userReducer = createReducer(
  initialUserState,
  on(UserActions.getDeManUser, (state) => {
    return {
      ...state,
      userCallState: LoadingState.LOADING,
    };
  }),
  on(UserActions.getDeManUserSuccess, (state, action) => {
    return {
      user: action.deManUser,
      userCallState: LoadingState.LOADED,
    };
  }),
  on(UserActions.getDeManUserFailed, (state, action) => {
    return {
      ...state,
      userCallState: { errorMessage: action.errorMessage },
    };
  }),
  on(UserActions.removeUser, () => initialUserState)
);
