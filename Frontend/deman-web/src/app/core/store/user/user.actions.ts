import { createAction, props } from '@ngrx/store';
import { User } from '@build/openapi';

export const getDeManUser = createAction(
  '[AuthService] Get DeMan-User after Login',
  props<{ firebaseUid: string; email: string }>()
);

export const registerDeManUser = createAction(
  '[DeMan User Service] Create user on registration',
  props<{ alias: string; firebaseUid: string; email: string }>()
);

export const getDeManUserSuccess = createAction(
  '[UserEffects] Get DeMan-User Succeed',
  props<{ deManUser: User }>()
);

export const getDeManUserFailed = createAction(
  '[UserEffects] Get DeMan-User Failed',
  props<{ errorMessage: string }>()
);

export const removeUser = createAction('[AuthService] Remove DeMan-User on logout');
