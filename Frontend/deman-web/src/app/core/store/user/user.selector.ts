import { createSelector } from '@ngrx/store';
import { selectUserState } from '@core/store';
import { getError, LoadingState } from '@shared/interfaces/call-state';

export const selectDeManUser = createSelector(selectUserState, (state) => state.user);

export const selectIsLoadingDeManUser = createSelector(
  selectUserState,
  (state) => state.userCallState === LoadingState.LOADING
);

export const selectDeManUserError = createSelector(selectUserState, (state) =>
  getError(state.userCallState)
);
