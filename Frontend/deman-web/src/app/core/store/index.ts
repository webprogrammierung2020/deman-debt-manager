import { ActionReducerMap, createFeatureSelector, MetaReducer } from '@ngrx/store';
import { authReducer, AuthState } from '@core/store/auth/auth.reducer';
import { routerReducer, RouterReducerState } from '@ngrx/router-store';
import { userReducer, UserState } from '@core/store/user/user.reducer';
import { logoutMetaReducer } from '@core/store/meta-reducer-functions/logout.meta-reducer';
import { transactionReducer, TransactionState } from '@core/store/transaction/transaction.reducer';

export interface CoreState {
  auth: AuthState;
  user: UserState;
  transaction: TransactionState;
  router: RouterReducerState;
}

export const reducers: ActionReducerMap<CoreState> = {
  auth: authReducer,
  user: userReducer,
  transaction: transactionReducer,
  router: routerReducer,
};

export const metaReducers: MetaReducer<CoreState>[] = [logoutMetaReducer];

export const selectAuthState = createFeatureSelector<AuthState>('auth');
export const selectUserState = createFeatureSelector<UserState>('user');
export const selectTransactionState = createFeatureSelector<TransactionState>('transaction');
