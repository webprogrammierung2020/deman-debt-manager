import { Action, ActionReducer } from '@ngrx/store';
import { CoreState } from '@core/store';

export function logoutMetaReducer(reducer: ActionReducer<CoreState>): ActionReducer<CoreState> {
  return (state: CoreState | undefined, action: Action): CoreState => {
    return reducer(action.type === '[Auth Service] Logout' ? undefined : state, action);
  };
}
