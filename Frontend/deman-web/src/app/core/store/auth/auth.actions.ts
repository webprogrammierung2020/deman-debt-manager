import { createAction, props } from '@ngrx/store';
import { AuthInfos } from '@shared/interfaces/auth-informations';
import { MatSnackBarRef, TextOnlySnackBar } from '@angular/material/snack-bar';
import { EmailVerificationHintComponent } from '@shared/components/email-verification-hint/email-verification-hint.component';

export const getFirebaseUser = createAction('[ROOT] Get Firebase User');

export const Unauthorized = createAction('[Auth Service] Unauthorized');

export const signUpRequest = createAction(
  '[SignUp Component] Signup Requested',
  props<{ authInfos: AuthInfos }>()
);

export const loginRequest = createAction(
  '[Login Component] Login Requested',
  props<{ authInfos: AuthInfos }>()
);

export const loginSuccess = createAction(
  '[Auth Service] Login Success',
  props<{ firebaseUid: string; email: string }>()
);

export const loginError = createAction(
  '[Auth Service] Login Error',
  props<{ errorMessage: string }>()
);

export const openEmailVerifiedSnack = createAction('[Auth Service] Opening Email Verified Modal');

export const closeEmailVerifiedSnack = createAction('[Auth Service] Closing Email Verified Modal');

export const logout = createAction('[Auth Service] Logout');
