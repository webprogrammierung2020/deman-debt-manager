import { CallState, LoadingState } from '@shared/interfaces/call-state';
import { createReducer, on } from '@ngrx/store';
import { AuthActions } from '@core/store/auth/auth-types';

export interface AuthState {
  isLoggedIn: boolean;
  callState: CallState;
  emailVerifiedModalIsOpen: boolean;
  registrationAlias: string;
}

export const initialState: AuthState = {
  isLoggedIn: false,
  callState: LoadingState.INIT,
  emailVerifiedModalIsOpen: false,
  registrationAlias: '',
};

export const authReducer = createReducer(
  initialState,
  on(AuthActions.signUpRequest, (state: AuthState, action) => {
    return {
      ...state,
      callState: LoadingState.LOADING,
      registrationAlias: action.authInfos.alias!,
    };
  }),
  on(AuthActions.loginRequest, (state: AuthState) => {
    return {
      ...state,
      callState: LoadingState.LOADING,
    };
  }),
  on(AuthActions.loginSuccess, (state: AuthState) => {
    return {
      ...state,
      isLoggedIn: true,
      callState: LoadingState.LOADED,
    };
  }),
  on(AuthActions.loginError, (state: AuthState, action) => {
    return {
      ...state,
      callState: { errorMessage: action.errorMessage },
    };
  }),
  on(AuthActions.openEmailVerifiedSnack, (state: AuthState) => {
    return {
      ...state,
      emailVerifiedModalIsOpen: true,
    };
  }),
  on(AuthActions.closeEmailVerifiedSnack, (state: AuthState) => {
    return {
      ...state,
      emailVerifiedModalIsOpen: false,
    };
  }),
  on(AuthActions.logout, () => {
    return initialState;
  })
);
