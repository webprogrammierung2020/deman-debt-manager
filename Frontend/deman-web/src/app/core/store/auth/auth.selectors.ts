import { createSelector } from '@ngrx/store';
import { selectAuthState } from '@core/store';
import { AuthState } from '@core/store/auth/auth.reducer';
import { getError, LoadingState } from '@shared/interfaces/call-state';

export const selectIsLoggedIn = createSelector(
  selectAuthState,
  (state: AuthState) => state.isLoggedIn
);

export const selectIsLoadingAuth = createSelector(
  selectAuthState,
  (state: AuthState) => state.callState === LoadingState.LOADING
);

export const selectAuthError = createSelector(selectAuthState, (state: AuthState) =>
  getError(state.callState)
);

export const selectEmailVerifiedSnackIsOpen = createSelector(
  selectAuthState,
  (state: AuthState) => state.emailVerifiedModalIsOpen
);

export const selectSignUpAlias = createSelector(
  selectAuthState,
  (state: AuthState) => state.registrationAlias
);
