import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { CommonModule } from '@angular/common';
import { NotificationDataService } from '@core/store/ngrx-data/notifications/notification-data.service';
import { NotificationEntityService } from '@core/store/ngrx-data/notifications/notification-entity.service';
import { EntityDataService, EntityDefinitionService } from '@ngrx/data';
import { TransactionEntityService } from '@core/store/ngrx-data/transactions/transaction-entity.service';
import { TransactionDataService } from '@core/store/ngrx-data/transactions/transaction-data.service';

@NgModule({
  imports: [CommonModule, StoreModule],
  providers: [
    NotificationDataService,
    NotificationEntityService,
    TransactionEntityService,
    TransactionDataService,
  ],
})
export class NgrxDataModule {
  constructor(
    private entityDataService: EntityDataService,
    private notificationDataService: NotificationDataService,
    private transactionDataService: TransactionDataService
  ) {
    entityDataService.registerService('Notification', notificationDataService);
    entityDataService.registerService('Transaction', transactionDataService);
  }
}
