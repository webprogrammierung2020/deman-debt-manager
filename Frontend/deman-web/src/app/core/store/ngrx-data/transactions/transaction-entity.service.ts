import { Injectable } from '@angular/core';
import { EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from '@ngrx/data';
import { Transaction } from '@build/openapi';

@Injectable()
export class TransactionEntityService extends EntityCollectionServiceBase<Transaction> {
  constructor(private serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super('Transaction', serviceElementsFactory);
  }
}
