import { Injectable } from '@angular/core';
import { DefaultDataService, HttpUrlGenerator, QueryParams } from '@ngrx/data';
import {
  Transaction,
  TransactionCategory,
  TransactionHistoryResponse,
  TransactionRole,
  TransactionService,
  UserService,
} from '@build/openapi';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '@core/services/auth.service';
import { filter, first, map, switchMap, take } from 'rxjs/operators';
import { notEmpty } from '@shared/utils/not-empty';

@Injectable()
export class TransactionDataService extends DefaultDataService<Transaction> {
  constructor(
    public http: HttpClient,
    public httpUrlGenerator: HttpUrlGenerator,
    private userService: UserService,
    private transactionService: TransactionService,
    private authService: AuthService
  ) {
    super('Transaction', http, httpUrlGenerator);
  }

  getWithQuery(queryParams: QueryParams | string): Observable<Transaction[]> {
    console.log('im in!');
    const role: TransactionRole = (queryParams as QueryParams).role as TransactionRole;
    const category: TransactionCategory = (queryParams as QueryParams)
      .category as TransactionCategory;
    const fbUid: string = (queryParams as QueryParams).fbUid as string;
    return this.userService.getTransactionHistByFirebaseUID(fbUid, category, role).pipe(
      map(
        (transactionHistResponse: TransactionHistoryResponse) =>
          transactionHistResponse.body.transaction_hist
      ),
      first()
    );
  }

  getAll(): Observable<Transaction[]> {
    return this.authService.firebaseUser$.pipe(
      filter(notEmpty),
      take(1),
      switchMap((fbUser) => {
        return this.userService
          .getTransactionHistByFirebaseUID(fbUser.uid)
          .pipe(map((transactionResponse) => transactionResponse.body.transaction_hist));
      }),
      first()
    );
  }
}
