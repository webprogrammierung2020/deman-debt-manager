import { Injectable } from '@angular/core';
import { EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from '@ngrx/data';
import { Notification } from '@build/openapi';

@Injectable()
export class NotificationEntityService extends EntityCollectionServiceBase<Notification> {
  constructor(private serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super('Notification', serviceElementsFactory);
  }
}
