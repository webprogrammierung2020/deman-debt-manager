import { Injectable } from '@angular/core';
import { DefaultDataService, HttpUrlGenerator, QueryParams } from '@ngrx/data';
import { Notification, NotificationsResponse, NotificationsService } from '@build/openapi';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '@core/services/auth.service';
import { filter, first, map, switchMap } from 'rxjs/operators';
import { notEmpty } from '@shared/utils/not-empty';
import firebase from 'firebase';

@Injectable()
export class NotificationDataService extends DefaultDataService<Notification> {
  constructor(
    public http: HttpClient,
    public httpUrlGenerator: HttpUrlGenerator,
    private notificationService: NotificationsService,
    private authService: AuthService
  ) {
    super('Notification', http, httpUrlGenerator);
  }

  // get notification by firebaseUid and role etc.
  getWithQuery(queryParams: QueryParams | string): Observable<Notification[]> {
    const fbUid: string = (queryParams as QueryParams).firebaseUid as string;
    return this.notificationService.findNotificationsForUserById(fbUid).pipe(
      map(
        (notificationResponse: NotificationsResponse) => notificationResponse.body.notification_list
      ),
      first()
    );
  }
}
