import { CallState, LoadingState } from '@shared/interfaces/call-state';
import { createReducer, on } from '@ngrx/store';
import { TransactionActions } from '@core/store/transaction/transaction-types';

export interface TransactionState {
  plainCallState: CallState;
  bucketCallState: CallState;
  debtCallState: CallState;
}

export const initialState: TransactionState = {
  plainCallState: LoadingState.INIT,
  bucketCallState: LoadingState.INIT,
  debtCallState: LoadingState.INIT,
};

export const transactionReducer = createReducer(
  initialState,
  on(TransactionActions.loadingPlainTransactionSucceed, (state) => {
    return {
      ...state,
      plainCallState: LoadingState.LOADED,
    };
  }),
  on(TransactionActions.loadingPlainError, (state, action) => {
    return {
      ...state,
      plainCallState: { errorMessage: action.errorMessage },
    };
  }),
  on(TransactionActions.loadingBucketTransactionSucceed, (state) => {
    return {
      ...state,
      bucketCallState: LoadingState.LOADED,
    };
  }),
  on(TransactionActions.loadingBucketError, (state, action) => {
    return {
      ...state,
      bucketCallState: { errorMessage: action.errorMessage },
    };
  }),
  on(TransactionActions.loadingDebtTransactionSucceed, (state) => {
    return {
      ...state,
      debtCallState: LoadingState.LOADED,
    };
  }),
  on(TransactionActions.loadingDebtError, (state, action) => {
    return {
      ...state,
      debtCallState: { errorMessage: action.errorMessage },
    };
  }),
  on(TransactionActions.resetForReload, () => initialState)
);
