import { createSelector } from '@ngrx/store';
import { selectTransactionState } from '@core/store';
import { getError, LoadingState } from '@shared/interfaces/call-state';

export const selectLoadedPlainTransactions = createSelector(
  selectTransactionState,
  (state) => state.plainCallState === LoadingState.LOADED
);

export const selectErrorPlain = createSelector(selectTransactionState, (state) =>
  getError(state.plainCallState)
);

export const selectLoadedBucketTransactions = createSelector(
  selectTransactionState,
  (state) => state.bucketCallState === LoadingState.LOADED
);

export const selectErrorBucket = createSelector(selectTransactionState, (state) =>
  getError(state.bucketCallState)
);

export const selectLoadedDebts = createSelector(
  selectTransactionState,
  (state) => state.debtCallState === LoadingState.LOADED
);

export const selectErrorDebt = createSelector(selectTransactionState, (state) =>
  getError(state.bucketCallState)
);
