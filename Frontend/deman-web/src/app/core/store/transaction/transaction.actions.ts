import { createAction, props } from '@ngrx/store';

export const loadingPlainTransactionSucceed = createAction('[Claim Overview] Loaded Plain');

export const loadingPlainError = createAction(
  '[Claim Overview] Loading Plain Error',
  props<{ errorMessage: string }>()
);

export const loadingBucketTransactionSucceed = createAction(
  '[Claim Overview] Loaded Bucket Succeed'
);

export const loadingBucketError = createAction(
  '[Claim Overview] Loading Bucket Error',
  props<{ errorMessage: string }>()
);

export const loadingDebtTransactionSucceed = createAction('[Claim Overview] Loaded Debt Succeed');

export const loadingDebtError = createAction(
  '[Claim Overview] Loading Debt Error',
  props<{ errorMessage: string }>()
);

export const resetForReload = createAction('[Claim Overview | Debt Overview] Reset Loaded States');
