import { SettingsComponent } from './components/settings/settings.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoComponent } from '@shared/components/demo/demo.component';
import { AuthGuard } from '@core/guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'auth', pathMatch: 'full' },
  { path: 'component-demo', component: DemoComponent },
  { path: 'settings', component: SettingsComponent },
  {
    path: 'dashboard',
    canLoad: [AuthGuard],
    loadChildren: () =>
      import('../deman/personal-dashboard/personal-dashboard.module').then(
        (m) => m.PersonalDashboardModule
      ),
  },
  {
    path: 'groups',
    canLoad: [AuthGuard],
    loadChildren: () =>
      import('../deman/group-dashboard/group-dashboard.module').then((m) => m.GroupDashboardModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoreRouting {}
