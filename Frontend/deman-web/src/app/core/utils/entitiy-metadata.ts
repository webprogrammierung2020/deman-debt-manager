import { EntityDispatcherDefaultOptions, EntityMetadataMap } from '@ngrx/data';

export const defaultEntityDispatcherOptions: Partial<EntityDispatcherDefaultOptions> = {
  optimisticAdd: false,
  optimisticUpsert: true,
  optimisticUpdate: true,
  optimisticDelete: true,
};

export const entityMetadataMap: EntityMetadataMap = {
  Notification: {
    selectId: (model) => model.notificationID,
    entityDispatcherOptions: defaultEntityDispatcherOptions,
  },
  Transaction: {
    selectId: (model) => model.transactionID,
    entityDispatcherOptions: defaultEntityDispatcherOptions,
  },
};
