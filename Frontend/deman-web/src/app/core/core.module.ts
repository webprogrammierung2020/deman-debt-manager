import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreRouting } from './core.routing';
import { MaterialModule } from './material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ApiModule } from '@build/openapi';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { metaReducers, reducers } from '@core/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { AuthService } from '@core/services/auth.service';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { EntityDataModule } from '@ngrx/data';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AuthInterceptor } from '@core/interceptors/http.interceptor';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SettingsComponent } from './components/settings/settings.component';
import { LegalNoticeComponent } from './components/settings/legal-notice/legal-notice.component';
import { SharedModule } from '@shared/shared.module';
import { DeManUserService } from '@core/services/de-man-user.service';
import { SidenavListComponent } from './components/sidenav/sidenav-list/sidenav-list.component';
import { NgrxDataModule } from '@core/store/ngrx-data/ngrx-data.module';
import { NotificationService } from '@core/services/notification.service';
import { entityMetadataMap } from '@core/utils/entitiy-metadata';
import { TransactionFacadeService } from '@core/services/transaction-facade.service';

@NgModule({
  declarations: [NavbarComponent, SettingsComponent, LegalNoticeComponent, SidenavListComponent],
  imports: [
    HttpClientModule,
    CommonModule,
    BrowserAnimationsModule,
    CoreRouting,
    MaterialModule,
    ApiModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreDevtoolsModule.instrument(environment.storeDevtoolsConfig),
    EffectsModule.forRoot([
      AuthService,
      DeManUserService,
      NotificationService,
      TransactionFacadeService,
    ]),
    StoreRouterConnectingModule.forRoot(environment.routerStoreConfig),
    EntityDataModule.forRoot({ entityMetadata: entityMetadataMap }),
    NgrxDataModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    SharedModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
  ],
  exports: [CommonModule, MaterialModule, NavbarComponent, SidenavListComponent],
})
export class CoreModule {}
