import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { forkJoin, from, Observable, of } from 'rxjs';
import { AuthService } from '@core/services/auth.service';
import { environment } from '../../../environments/environment';
import { filter, switchMap } from 'rxjs/operators';
import firebase from 'firebase';
import { notEmpty } from '@shared/utils/not-empty';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(public auth: AuthService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.auth.firebaseUser$.pipe(
      filter(notEmpty),
      switchMap((firebaseUser: firebase.User) => from(firebaseUser.getIdToken())),
      switchMap((token: string) => {
        request = request.clone({
          setHeaders: {
            Authorization: token,
            'x-api-key': environment.xApiKey,
          },
        });
        return next.handle(request);
      })
    );
  }
}
