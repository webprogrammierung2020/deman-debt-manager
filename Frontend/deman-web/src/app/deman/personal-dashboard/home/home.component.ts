import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '@build/openapi';
import { DeManUserService } from '@core/services/de-man-user.service';

@Component({
  selector: 'pd-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  user$: Observable<User> = this.deManUserService.user$;

  constructor(private deManUserService: DeManUserService) {}
}
