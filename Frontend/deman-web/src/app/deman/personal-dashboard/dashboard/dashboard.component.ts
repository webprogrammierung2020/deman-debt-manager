import { Component, OnInit } from '@angular/core';
import { TransactionEntityService } from '@core/store/ngrx-data/transactions/transaction-entity.service';
import { combineLatest, Observable, of } from 'rxjs';
import {
  Transaction,
  TransactionParticipants,
  User,
  UserResponse,
  UserService,
} from '@build/openapi';
import { UntilDestroy } from '@ngneat/until-destroy';
import { filter, map, switchMap, take, withLatestFrom } from 'rxjs/operators';
import { notEmpty } from '@shared/utils/not-empty';
import { DeManUserService } from '@core/services/de-man-user.service';

@UntilDestroy()
@Component({
  selector: 'pd-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  currentUser: User;
  relatedUsers$: Observable<User[]>;

  constructor(
    private userService: UserService,
    private deManUserService: DeManUserService,
    private transactionEntityService: TransactionEntityService
  ) {}

  ngOnInit(): void {
    this.relatedUsers$ = this.transactionEntityService.loaded$.pipe(
      take(1),
      withLatestFrom(this.deManUserService.user$),
      map(([loaded, user]: [boolean, User]) => {
        this.currentUser = user;
        return loaded;
      }),
      switchMap((loaded) => {
        if (!loaded) {
          return this.transactionEntityService.getAll();
        } else {
          return of(null);
        }
      }),
      filter(notEmpty),
      switchMap((transactions) => {
        return this.extractRelatedUsers(transactions).pipe(
          map((userResponses) => userResponses.map((userResponse) => userResponse.body.user))
        );
      }),
      take(1)
    );
  }

  private extractRelatedUsers(transactions: Transaction[]): Observable<UserResponse[]> {
    const usersToExtract: Observable<UserResponse>[] = [];
    const addedUserIds: string[] = [];
    transactions.forEach((transaction: Transaction) => {
      if (
        transaction.creditor !== this.currentUser.firebaseUID &&
        !addedUserIds.includes(transaction.creditor)
      ) {
        usersToExtract.push(this.userService.getUserByFirebaseUID(transaction.creditor));
        addedUserIds.push(transaction.creditor);
      }
      transaction.participants.forEach((participant: TransactionParticipants) => {
        if (
          participant.debtor !== this.currentUser.firebaseUID &&
          !addedUserIds.includes(participant.debtor)
        ) {
          usersToExtract.push(this.userService.getUserByFirebaseUID(participant.debtor));
          addedUserIds.push(participant.debtor);
        }
      });
    });
    return combineLatest(usersToExtract);
  }
}
