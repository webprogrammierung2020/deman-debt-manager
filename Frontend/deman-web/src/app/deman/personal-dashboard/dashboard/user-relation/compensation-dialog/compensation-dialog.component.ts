import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-compensation-dialog',
  templateUrl: './compensation-dialog.component.html',
  styleUrls: ['./compensation-dialog.component.scss'],
})
export class CompensationDialogComponent implements OnInit {
  compensationForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<CompensationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { outstandingBalance: number }
  ) {}

  ngOnInit(): void {
    this.compensationForm = new FormGroup({
      amount: new FormControl(1, [
        Validators.required,
        Validators.min(1),
        Validators.max(this.data.outstandingBalance),
      ]),
      description: new FormControl('', [Validators.required, Validators.minLength(10)]),
    });
  }

  onCompensation(): void {
    if (this.compensationForm.get('amount')!.valid) {
      this.dialogRef.close({
        amountToCompensate: this.compensationForm.get('amount')!.value,
        description: this.compensationForm.get('description')!.value,
      });
    }
  }
}
