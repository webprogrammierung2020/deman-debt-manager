import { Component, Input, OnInit } from '@angular/core';
import {
  BalanceResponseBody,
  Transaction,
  TransactionParticipants,
  TransactionService,
  User,
  UserService,
} from '@build/openapi';
import { Observable } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';
import { TransactionFacadeService } from '@core/services/transaction-facade.service';
import { toTableEntry } from '@shared/utils/to-table-entry';
import { ReadableTransactionTableItem } from '@shared/interfaces/readable-transaction-table-item';
import { notEmpty } from '@shared/utils/not-empty';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CompensationDialogComponent } from '@personal-dashboard/dashboard/user-relation/compensation-dialog/compensation-dialog.component';
import { Router } from '@angular/router';
import { SnackbarService } from '@core/services/snackbar.service';

@Component({
  selector: 'pd-user-relation',
  templateUrl: './user-relation.component.html',
  styleUrls: ['./user-relation.component.scss'],
})
export class UserRelationComponent implements OnInit {
  @Input() currentUser: User;
  @Input() relatedUser: User;

  balance$: Observable<BalanceResponseBody>;
  relatedClaims$: Observable<ReadableTransactionTableItem[]> = this.transactionFacade.claims$.pipe(
    map((transactions: Transaction[]) => {
      return transactions.filter((transaction: Transaction) =>
        transaction.participants.some(
          (participant: TransactionParticipants) =>
            participant.debtor === this.relatedUser.firebaseUID
        )
      );
    }),
    map((filteredTransactions) => ({
      transactions: filteredTransactions,
      comparableUser: this.relatedUser,
    })),
    map(toTableEntry),
    filter(notEmpty)
  );

  relatedDebts$: Observable<ReadableTransactionTableItem[]> = this.transactionFacade.debts$.pipe(
    map((transactions: Transaction[]) =>
      transactions.filter(
        (transaction: Transaction) => transaction.creditor === this.relatedUser.firebaseUID
      )
    ),
    map((filteredTransactions) => ({
      transactions: filteredTransactions,
      comparableUser: this.currentUser,
    })),
    map(toTableEntry),
    filter(notEmpty)
  );

  constructor(
    private userService: UserService,
    private transactionFacade: TransactionFacadeService,
    private dialog: MatDialog,
    private transactionService: TransactionService,
    private router: Router,
    private snackBarService: SnackbarService
  ) {}

  ngOnInit(): void {
    console.log(this.relatedUser);
    this.balance$ = this.userService
      .getBalanceWithUserByFirebaseUID(this.currentUser.firebaseUID, this.relatedUser.firebaseUID)
      .pipe(map((balanceResponse) => balanceResponse.body));
  }

  getColorBaseOn(balance: string): string {
    return parseFloat(balance) >= 0 ? 'green' : 'red';
  }

  getSubtitle(balance: string): string {
    return parseFloat(balance) >= 0
      ? `${this.relatedUser.username} schuldet dir momentan Geld`
      : `Du Schuldest ${this.relatedUser.username} Geld (Du kannst eine Ausgleichstransaktion erstellen)`;
  }

  userNeedsToCompensate(balance: string): boolean {
    return parseFloat(balance) < 0;
  }

  onCompensate(balance: string): void {
    const dialogRef: MatDialogRef<CompensationDialogComponent> = this.dialog.open(
      CompensationDialogComponent,
      { data: { outstandingBalance: Math.abs(parseFloat(balance)) } }
    );
    dialogRef
      .afterClosed()
      .subscribe((result: { amountToCompensate: string; description: string } | undefined) => {
        console.log(result);
        if (result !== undefined) {
          console.log(result);
          this.transactionService
            .addCompensaton({
              creditor: this.relatedUser.firebaseUID,
              amount: '-' + result.amountToCompensate,
              description: result.description,
              participants: [{ debtor: this.currentUser.firebaseUID }],
            })
            .pipe(
              map((transactionResponse) => transactionResponse.body.transaction),
              take(1)
            )
            .subscribe((transaction) => {
              console.log('Compensated!!!!');
              console.log(transaction);
              this.router
                .navigateByUrl('dashboard')
                .then(() =>
                  this.snackBarService.openDefaultSnack(
                    'Ausgleichtransaktion erfolgreich Erstellt! Der beteiligte User muss sie nur noch akzeptieren!',
                    'Ok',
                    { duration: 5000 }
                  )
                );
            });
        }
      });
  }
}
