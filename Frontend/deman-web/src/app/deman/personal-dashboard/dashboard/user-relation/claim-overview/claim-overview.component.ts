import { Component, Input } from '@angular/core';
import { ReadableTransactionTableItem } from '@shared/interfaces/readable-transaction-table-item';

@Component({
  selector: 'pd-claim-overview',
  templateUrl: './claim-overview.component.html',
  styleUrls: ['./claim-overview.component.scss'],
})
export class ClaimOverviewComponent {
  displayedColumns: string[] = ['Erstellt', 'Beschreibung', 'Summe'];

  @Input() relatedClaims: ReadableTransactionTableItem[];

  constructor() {}
}
