import { Component, Input } from '@angular/core';
import { ReadableTransactionTableItem } from '@shared/interfaces/readable-transaction-table-item';

@Component({
  selector: 'pd-debt-overview',
  templateUrl: './debt-overview.component.html',
  styleUrls: ['./debt-overview.component.scss'],
})
export class DebtOverviewComponent {
  displayedColumns: string[] = ['Erstellt', 'Beschreibung', 'Summe'];

  @Input() relatedDebts: ReadableTransactionTableItem[];

  constructor() {}
}
