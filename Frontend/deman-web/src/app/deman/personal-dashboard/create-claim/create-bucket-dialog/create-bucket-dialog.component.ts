import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { AdvancedDialogOptions } from '@personal-dashboard/create-claim/create-claim.component';
import { SnackbarService } from '@core/services/snackbar.service';

@Component({
  selector: 'app-create-bucket-dialog',
  templateUrl: './create-bucket-dialog.component.html',
  styleUrls: ['./create-bucket-dialog.component.scss'],
})
export class CreateBucketDialogComponent implements OnInit {
  createBucketFormGroup: FormGroup;

  get participantsControl(): FormArray {
    return this.createBucketFormGroup.get('participants') as FormArray;
  }

  constructor(
    public dialogRef: MatDialogRef<CreateBucketDialogComponent>,
    // TODO Refactor in openAPI yaml (to component)
    @Inject(MAT_DIALOG_DATA) public data: AdvancedDialogOptions[],
    private snackBarService: SnackbarService
  ) {}

  ngOnInit(): void {
    this.createBucketFormGroup = new FormGroup({
      participants: new FormArray(this.createParticipantsControls()),
    });
    console.log(this.participantsControl);
  }

  private createParticipantsControls(): FormControl[] {
    const formGroupArray: FormControl[] = [];
    this.data.forEach(() => {
      formGroupArray.push(
        new FormControl(
          {
            share: 0,
          },
          Validators.required
        )
      );
    });
    return formGroupArray;
  }

  onCreateBucketTransaction(): void {
    console.log(this.participantsControl.value);
    const sum = (this.participantsControl.value as number[]).reduce(
      (acc: number, val: number) => acc + val,
      0
    );
    console.log(sum);
    console.log(this.data[0].aggregatedShare);
    if (sum == this.data[0].aggregatedShare) {
      console.log('ready to create transaction!');
      this.dialogRef.close(this.participantsControl.value);
    } else {
      this.snackBarService.openDefaultSnack(
        'Die Anteile müssen den Gesamtwert ergeben!',
        'Schließen',
        { duration: 5000 }
      );
    }
  }

  onKeepEditing(): void {
    this.dialogRef.close('KEEP_EDITING');
  }
}
