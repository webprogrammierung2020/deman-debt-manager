import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import {
  InlineObject4,
  TransactionResponse,
  TransactionsBucketParticipants,
  TransactionService,
  User,
  UserService,
} from '@build/openapi';
import { COMMA, ENTER, TAB } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { take } from 'rxjs/operators';
import { TransactionEntityService } from '@core/store/ngrx-data/transactions/transaction-entity.service';
import { SnackbarService } from '@core/services/snackbar.service';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

export interface AdvancedDialogOptions extends TransactionsBucketParticipants {
  email: string;
  aggregatedShare: number;
}

@Component({
  selector: 'pd-create-claim',
  templateUrl: './create-claim.component.html',
  styleUrls: ['./create-claim.component.scss'],
})
export class CreateClaimComponent implements OnInit {
  createClaimForm: FormGroup;
  minDate = new Date();
  maxDate = new Date().getDate() + 10;

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA, TAB];

  loading = false;
  addingTransaction = false;

  creditorHint = 'Geb die Email eines Users ein und drücke Enter!';
  debtorHint = ['Geb die Email eines Users ein und drücke Enter'];

  creditors: User[] = [];
  debtors: Partial<User>[] = [{}];
  creditorInput = '';
  debtorInput = '';

  constructor(
    private userService: UserService,
    private transactionService: TransactionService,
    private transactionEntityService: TransactionEntityService,
    private snackbarService: SnackbarService,
    private matDialog: MatDialog,
    private router: Router
  ) {}

  get amountFormArray(): FormArray {
    return this.createClaimForm.get('amounts') as FormArray;
  }

  ngOnInit(): void {
    this.createClaimForm = new FormGroup({
      amounts: new FormArray([new FormControl(1, [Validators.required, Validators.min(1)])]),
      description: new FormControl('', [Validators.required, Validators.maxLength(200)]),
    });
  }

  resetCreateClaimForm(): void {
    this.createClaimForm.reset();
  }

  removeCreditor(): void {
    this.creditors = [];
    this.creditorHint = 'User wurde gelöscht, du kannst nun weitere User wählen!';
  }

  addCreditor($event: MatChipInputEvent): void {
    if (!this.isUnique($event.value)) {
      this.creditorHint = 'Der User wurde bereits eingegeben!';
      return;
    }
    this.loading = true;
    this.creditorHint = 'Prüden, ob der User existiert...';
    this.userService
      .getUserInfoByEmail($event.value)
      .pipe(take(1))
      .subscribe(
        (userResponse) => {
          this.creditors.push(userResponse.body.user);
          this.creditorHint = 'User wurde hinzugefügt!';
        },
        (error) => {
          this.creditorHint =
            'Der angegebene User scheint nicht zu exisiteren, bitte check deine Eingabe und versuch es nochmal!';
        },
        () => (this.loading = false)
      );
  }

  removeDebtor(debtor: Partial<User>): void {
    const index = this.debtors.indexOf(debtor);
    if (this.debtors.length === 1) {
      this.debtors[0] = {};
      this.debtorHint[0] = 'Geb die Email eines Users ein und drücke Enter!';
      return;
    }
    if (index >= 0) {
      this.debtors.splice(index, 1);
      this.debtorHint.splice(index, 1);
      this.amountFormArray.removeAt(index);
    }
  }

  addDebtor(): void {
    if (this.debtors[this.debtors.length - 1].email !== undefined) {
      this.amountFormArray.push(new FormControl(1, [Validators.required, Validators.min(1)]));
      this.debtors.push({});
      this.debtorHint.push('Geb die Email eines Users ein und drücke Enter');
    } else {
      this.snackbarService.openDefaultSnack(
        'Bitte geben sie erst einen Schuldner in dem vorhandenen Feld ein!',
        'Ok',
        { duration: 5000 }
      );
    }
  }

  isUnique(email: string): boolean {
    return !(
      this.creditors.find((user) => user.email === email) ||
      this.debtors.find((user) => user.email === email)
    );
  }

  onCreateTransaction(): void {
    if (
      this.creditors.length === 1 &&
      this.createClaimForm.valid &&
      this.debtors.length !== 0 &&
      this.noEmptyParticipants()
    ) {
      if (this.debtors.length === 1) {
        console.log(this.createClaimForm);
        console.log(this.debtors);
        console.log(this.creditors);
        // create plain transaction
        this.addingTransaction = true;
        this.transactionService
          .addPlaintransation({
            creditor: this.creditors[0].firebaseUID,
            amount: this.amountFormArray.at(0).value,
            description: this.createClaimForm.get('description')!.value,
            participants: [
              {
                debtor: this.debtors[0].firebaseUID,
              },
            ],
          })
          .pipe(take(1))
          .subscribe(
            (createdTransactionResponse: TransactionResponse) => {
              // add created transaction to cache
              this.onCreateSuccess(createdTransactionResponse);
            },
            (error) => {
              this.onCreateError(error);
            },
            () => (this.addingTransaction = false)
          );
      } else {
        this.addingTransaction = true;
        const participants: TransactionsBucketParticipants[] = this.debtors.map(
          (user: Partial<User>, index: number) => ({
            debtor: user.firebaseUID,
            share: (this.amountFormArray.at(index).value as number).toString(),
          })
        );
        const transactionToUpsert: InlineObject4 = {
          creditor: this.creditors[0].firebaseUID,
          amount: this.amountFormArray.value
            .reduce((acc: number, val: string) => acc + parseInt(val, 10), 0)
            .toString(),
          description: this.createClaimForm.get('description')?.value!,
          participants,
        };
        console.log(transactionToUpsert);
        this.transactionService
          .addBuckettransation(transactionToUpsert)
          .pipe(take(1))
          .subscribe(
            (addedTransaction) => {
              // add created transaction to cache
              this.onCreateSuccess(addedTransaction);
            },
            (error) => {
              this.onCreateError(error);
            },
            () => (this.addingTransaction = false)
          );
      }
    } else {
      console.log('Bitte gebe in jedes Feld mindestens einen Wert ein!');
    }
  }

  private onCreateSuccess(addedTransaction: TransactionResponse): void {
    this.transactionEntityService.addOneToCache(addedTransaction.body.transaction);
    this.router.navigateByUrl('').then(() => {
      this.snackbarService.openDefaultSnack(
        'Deine Transaction wurde erstellt. Eine Nachricht wurde an den Beteiligten gesendet!',
        'Schließen',
        { duration: 5000 }
      );
    });
  }

  private onCreateError(error: HttpErrorResponse): void {
    this.snackbarService.openDefaultSnack(
      'Beim Erstellen deiner Transaktion ist ein Fehler aufgretreten bitte versuche es später erneut!',
      'Schließen',
      { duration: 5000 }
    );
    console.log(error);
  }

  addDebtorIntoChip($event: MatChipInputEvent, i: number): void {
    if (!this.isUnique($event.value)) {
      this.debtorHint[i] = 'Der User wurde bereits eingegeben!';
      return;
    }
    this.debtors[i] = {
      email: $event.value,
    };
    this.loading = true;
    this.debtorHint[i] = 'Prüden, ob der User existiert...';
    this.userService
      .getUserInfoByEmail($event.value)
      .pipe(take(1))
      .subscribe(
        (userResponse) => {
          this.debtors[i] = userResponse.body.user;
          this.debtorHint[i] = 'User wurde hinzugefügt, du kannst nun weitere User wählen!';
        },
        (error) => {
          this.debtorHint[i] =
            'Der angegebene User scheint nicht zu exisiteren, bitte check deine Eingabe und versuch es nochmal!';
        },
        () => (this.loading = false)
      );
  }

  private noEmptyParticipants(): boolean {
    return (
      !this.debtors.some(
        (debtor) => debtor && Object.keys(debtor).length === 0 && debtor.constructor === Object
      ) &&
      !(Object.keys(this.creditors[0]).length === 0 && this.creditors[0].constructor === Object)
    );
  }
}
