import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { DebtOverviewComponent } from '@personal-dashboard/dashboard/user-relation/debt-overview/debt-overview.component';
import { CreateClaimComponent } from '@personal-dashboard/create-claim/create-claim.component';
import { ClaimDebtDetailViewComponent } from '@personal-dashboard/claim-debt-detailview/claim-debt-detail-view.component';
import { AuthGuard } from '@core/guards/auth.guard';
import { EntryComponent } from '@personal-dashboard/entry/entry.component';
import { DashboardComponent } from '@personal-dashboard/dashboard/dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: EntryComponent,
    canActivateChild: [AuthGuard],
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: HomeComponent },
      { path: 'debt-overview', component: DebtOverviewComponent },
      { path: 'create-claim', component: CreateClaimComponent },
      { path: 'transaction-overview', component: DashboardComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PersonalDashboardRouting {}
