import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { PersonalDashboardRouting } from './personal-dashboard.routing';
import { DebtOverviewComponent } from './dashboard/user-relation/debt-overview/debt-overview.component';
import { ClaimOverviewComponent } from './dashboard/user-relation/claim-overview/claim-overview.component';
import { ClaimDebtDetailViewComponent } from './claim-debt-detailview/claim-debt-detail-view.component';
import { CreateClaimComponent } from './create-claim/create-claim.component';
import { EntryComponent } from './entry/entry.component';
import { MaterialModule } from '@core/material.module';
import { ChartsModule } from 'ng2-charts';
import { CreateBucketDialogComponent } from './create-claim/create-bucket-dialog/create-bucket-dialog.component';
import { CompensationDialogComponent } from './dashboard/user-relation/compensation-dialog/compensation-dialog.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserRelationComponent } from './dashboard/user-relation/user-relation.component';

@NgModule({
  declarations: [
    HomeComponent,
    DebtOverviewComponent,
    ClaimOverviewComponent,
    ClaimDebtDetailViewComponent,
    CreateClaimComponent,
    EntryComponent,
    CreateBucketDialogComponent,
    CompensationDialogComponent,
    DashboardComponent,
    UserRelationComponent,
  ],
  imports: [
    CommonModule,
    PersonalDashboardRouting,
    SharedModule,
    ReactiveFormsModule,
    MaterialModule,
    ChartsModule,
    FormsModule,
  ],
})
export class PersonalDashboardModule {}
