import { Component } from '@angular/core';

@Component({
  selector: 'pt-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.scss'],
})
export class EntryComponent {}
