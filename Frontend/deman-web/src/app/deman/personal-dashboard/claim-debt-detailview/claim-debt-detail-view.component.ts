import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Transaction, TransactionService, UserResponse, UserService } from '@build/openapi';
import { TransactionEntityService } from '@core/store/ngrx-data/transactions/transaction-entity.service';
import { map, switchMap, take, tap } from 'rxjs/operators';
import { FormControl, FormGroup } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { AuthService } from '@core/services/auth.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CompensationDialogComponent } from '@personal-dashboard/dashboard/user-relation/compensation-dialog/compensation-dialog.component';

@Component({
  selector: 'pd-claim-debt-detailView',
  templateUrl: './claim-debt-detail-view.component.html',
  styleUrls: ['./claim-debt-detail-view.component.scss'],
})
export class ClaimDebtDetailViewComponent implements OnInit {
  transaction: Transaction;
  role: 'creditor' | 'debtor';
  detailClaimForm: FormGroup;
  loading: boolean;
  debtor: string;

  constructor(
    private route: ActivatedRoute,
    private transactionEntityService: TransactionEntityService,
    private authService: AuthService,
    private userService: UserService,
    private dialog: MatDialog,
    private transactionService: TransactionService
  ) {}

  ngOnInit(): void {
    this.loading = true;
    this.route.queryParams
      .pipe(
        map((queryParams) => ({
          transactionId: parseInt(queryParams['transactionId'], 10),
          role: queryParams['role'],
        })),
        tap((params) => {
          this.role = params.role;
          if (this.role === 'debtor') {
            this.debtor = this.authService.firebaseUser$.getValue()!.uid;
          }
        }),
        switchMap((params) => {
          return this.transactionEntityService.entities$.pipe(
            map((transactions: Transaction[]) => {
              console.log(transactions);
              return transactions.find(
                (transaction: Transaction) => transaction.transactionID === params.transactionId
              )!;
            })
          );
        }),
        switchMap((transaction) => {
          this.transaction = transaction;
          return this.userService.getUserByFirebaseUID(transaction.creditor);
        }),
        take(1)
      )
      .subscribe(
        (userResponse: UserResponse) => {
          const datePipe = new DatePipe('de');
          const creditor = userResponse.body.user.email;
          this.detailClaimForm = new FormGroup({
            creditor: new FormControl(creditor),
            createdAt: new FormControl(datePipe.transform(this.transaction.createdAt)),
            amount: new FormControl(this.resolveAmount()),
            description: new FormControl(this.transaction.description!),
          });
          this.loading = false;
        },
        (error) => (this.loading = false)
      );
  }

  private resolveAmount(): number {
    if (this.transaction.participants.length > 1 && this.role === 'debtor') {
      const user = this.authService.firebaseUser$.getValue()!;
      return this.transaction.participants.find((participant) => participant.debtor === user.uid)!
        .share!;
    } else {
      return parseFloat(this.transaction.amount);
    }
  }

  onCompensation(): void {
    const dialogRef: MatDialogRef<CompensationDialogComponent> = this.dialog.open(
      CompensationDialogComponent,
      {
        data: { share: this.detailClaimForm.get('amount')!.value! },
      }
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result as number) {
        console.log('is number');
        console.log(result);
        this.transactionService
          .addCompensaton({
            amount: `-${result}`,
            description: this.transaction.description,
            creditor: this.transaction.creditor,
            participants: [{ debtor: this.debtor }],
          })
          .pipe(
            map((transactionResponse) => transactionResponse.body.transaction),
            take(1)
          )
          .subscribe((transaction) => console.log(transaction));
      }
    });
  }
}
