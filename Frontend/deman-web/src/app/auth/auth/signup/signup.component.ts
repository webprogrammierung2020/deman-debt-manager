import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '@core/services/auth.service';
import { AuthInfos } from '@shared/interfaces/auth-informations';
import { combineLatest, Observable } from 'rxjs';
import { UserService } from '@build/openapi';
import { DeManUserService } from '@core/services/de-man-user.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'auth-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  loading$: Observable<boolean> = combineLatest([
    this.authService.loading$,
    this.deManUserService.loading$,
  ]).pipe(map(([loadingAuth, loadingUser]: [boolean, boolean]) => loadingAuth && loadingUser));

  get usernameFormControl(): AbstractControl {
    return this.signupForm.get('username')!;
  }

  get firstnameFormControl(): AbstractControl {
    return this.signupForm.get('firstName')!;
  }

  get lastnameFormControl(): AbstractControl {
    return this.signupForm.get('lastName')!;
  }

  get passwordFormControl(): AbstractControl {
    return this.signupForm.get('password')!;
  }

  get emailFormControl(): AbstractControl {
    return this.signupForm.get('email')!;
  }

  get rememberMeFormControl(): AbstractControl {
    return this.signupForm.get('rememberMe')!;
  }

  constructor(private authService: AuthService, private deManUserService: DeManUserService) {}

  ngOnInit(): void {
    this.signupForm = new FormGroup({
      username: new FormControl('', [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(20),
      ]),
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      password: new FormControl('', [Validators.required, Validators.minLength(10)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      rememberMe: new FormControl(false),
    });
  }

  get authInfos(): AuthInfos {
    return {
      alias: this.removeAllWhitespaces(this.usernameFormControl.value),
      password: this.removeAllWhitespaces(this.passwordFormControl.value),
      email: this.removeAllWhitespaces(this.emailFormControl.value),
      firstname: this.removeAllWhitespaces(this.firstnameFormControl.value),
      lastname: this.removeAllWhitespaces(this.lastnameFormControl.value),
      rememberMe: this.rememberMeFormControl.value,
    };
  }

  removeAllWhitespaces(input: string): string {
    return input.replace(/\s/g, '');
  }

  onSingUp(): void {
    if (this.signupForm.valid) {
      console.log(this.authInfos);
      this.authService.signUp(this.authInfos);
    }
  }
}
