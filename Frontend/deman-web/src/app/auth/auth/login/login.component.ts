import { SnackbarService } from '@core/services/snackbar.service';
import { filter } from 'rxjs/operators';
import { notEmpty } from '@shared/utils/not-empty';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '@core/services/auth.service';
import { AuthInfos } from '@shared/interfaces/auth-informations';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ResetPasswordComponent } from '@shared/components/reset-password/reset-password.component';

@Component({
  selector: 'auth-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading$: Observable<boolean> = this.authService.loading$;
  error$: Observable<string> = this.authService.error$.pipe(filter(notEmpty));
  hasError = false;

  get emailFormControl(): AbstractControl {
    return this.loginForm.get('email')!;
  }

  get passwordFormControl(): AbstractControl {
    return this.loginForm.get('password')!;
  }

  get rememberMeFormControl(): AbstractControl {
    return this.loginForm.get('rememberMe')!;
  }

  get authInfos(): AuthInfos {
    return {
      email: this.removeAllWhitespaces(this.emailFormControl.value),
      password: this.removeAllWhitespaces(this.passwordFormControl.value),
      rememberMe: this.rememberMeFormControl.value,
    };
  }

  removeAllWhitespaces(input: string): string {
    return input.replace(/\s/g, '');
  }

  constructor(
    private authService: AuthService,
    private dialog: MatDialog,
    private snackbarService: SnackbarService
  ) {}

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl(''),
      password: new FormControl(''),
      rememberMe: new FormControl(false),
    });
    this.error$.subscribe((error) => {
      this.emailFormControl.setErrors({ required: true });
      this.passwordFormControl.setErrors({ required: true });
      this.snackbarService.openDefaultSnack('Emailadresse und/oder Passwort falsch.', 'OK', {
        duration: 5000,
      });
      this.hasError = true;
    });
  }

  onLogin(): void {
    this.authService.login(this.authInfos);
  }

  openPasswordResetModal(): void {
    const dialogRef = this.dialog.open(ResetPasswordComponent, {
      width: '500px',
    });
  }
}
