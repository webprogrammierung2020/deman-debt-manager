import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { RouterModule } from '@angular/router';
import { AuthRouting } from './auth.routing';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@core/material.module';

@NgModule({
  declarations: [AuthComponent, LoginComponent, SignupComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    AuthRouting,
    ReactiveFormsModule,
    MaterialModule,
  ],
})
export class AuthModule {}
