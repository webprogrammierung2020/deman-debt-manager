import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './components/button/button.component';
import { ClaimListComponent } from './components/claim-list/claim-list.component';
import { DefaultTextInputComponent } from './components/default-textinput/default-text-input.component';
import { DefaultTextAreaComponent } from './components/default-text-area/default-text-area.component';
import { DemoComponent } from './components/demo/demo.component';
import { MaterialModule } from '@core/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoadingButtonDirective } from './directives/loading-button.directive';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { EmailVerificationHintComponent } from './components/email-verification-hint/email-verification-hint.component';
import { NotificationListComponent } from './components/notification-list/notification-list.component';
import { SingleNotificationComponent } from './components/notification-list/single-notification/single-notification.component';
import { PreventKeysDirective } from './directives/prevent-keys.directive';
import { NotificationActionsComponent } from './components/notification-list/single-notification/notification-actions/notification-actions.component';
import { ExtractAnswerPipe } from './pipes/extract-anwser.pipe';

@NgModule({
  declarations: [
    ButtonComponent,
    ClaimListComponent,
    DefaultTextInputComponent,
    DefaultTextAreaComponent,
    DemoComponent,
    LoadingButtonDirective,
    ResetPasswordComponent,
    EmailVerificationHintComponent,
    NotificationListComponent,
    SingleNotificationComponent,
    PreventKeysDirective,
    NotificationActionsComponent,
    ExtractAnswerPipe,
  ],
  imports: [CommonModule, MaterialModule, ReactiveFormsModule, FormsModule],
  exports: [
    CommonModule,
    ButtonComponent,
    ClaimListComponent,
    DefaultTextInputComponent,
    DefaultTextAreaComponent,
    PreventKeysDirective,
  ],
  entryComponents: [ResetPasswordComponent, NotificationListComponent],
})
export class SharedModule {}
