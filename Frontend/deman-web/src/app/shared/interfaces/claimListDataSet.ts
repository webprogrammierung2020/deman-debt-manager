export interface claimListDataSet {
  transactionId: number;
  createdBy: number;
  createdAt: string;
  content: string;
  creditor: number;
  sum: number;
  debitor: number;
  acknowlegded: number;
}
