export interface AuthInfos {
  alias?: string;
  firstname?: string;
  lastname?: string;
  rememberMe: boolean;
  email: string;
  password: string;
}
