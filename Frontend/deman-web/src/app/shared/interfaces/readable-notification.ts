export interface ReadableNotification {
  title: string;
  body: string;
  isResponse: boolean;
}

export enum NotificationPurpose {
  CREATE = 'create',
  RESPOND = 'response',
}
