export interface ReadableTransactionTableItem {
  Erstellt: string;
  Beschreibung: string;
  Summe: string;
  id: number;
}
