export interface UserRelationInfos {
  balance: number;
  relatedUserId: string;
}
