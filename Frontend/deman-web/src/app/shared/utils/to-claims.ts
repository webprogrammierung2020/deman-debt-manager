import { Transaction } from '@build/openapi';
import firebase from 'firebase';

export function toClaims([fbUser, transactions]: [firebase.User, Transaction[]]): Transaction[] {
  return transactions.filter((transaction) => transaction.creditor === fbUser.uid);
}
