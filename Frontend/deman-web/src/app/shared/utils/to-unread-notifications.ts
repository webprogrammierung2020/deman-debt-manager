import { Notification } from '@build/openapi';

export function toUnreadNotification(notifications: Notification[]): Notification[] {
  return notifications.filter((notification) => !notification.isRead);
}
