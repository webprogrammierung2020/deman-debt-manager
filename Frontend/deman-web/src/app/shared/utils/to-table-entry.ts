import { Transaction, User } from '@build/openapi';
import { ReadableTransactionTableItem } from '@shared/interfaces/readable-transaction-table-item';
import { DatePipe } from '@angular/common';

export function toTableEntry(mappingOptions: {
  transactions: Transaction[];
  comparableUser: User;
}): ReadableTransactionTableItem[] {
  console.log(mappingOptions.transactions);
  const pipe = new DatePipe('de');
  return mappingOptions.transactions.map((transaction: Transaction) => {
    const createdAt = pipe.transform(transaction.createdAt!);
    const sum: string =
      transaction.participants.length >= 1
        ? `${
            transaction.participants.find(
              (participant) => participant.debtor === mappingOptions.comparableUser.firebaseUID
            )!.share
          }`
        : transaction.amount;
    return {
      Beschreibung: transaction.description!,
      Erstellt: createdAt!,
      Summe: sum,
      id: transaction.transactionID!,
    };
  });
}
