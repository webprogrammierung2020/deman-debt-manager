import { Transaction } from '@build/openapi';
import firebase from 'firebase';

// Check if the current user is debtor in any transactions in the cache
export function toDebts([fbUser, transactions]: [firebase.User, Transaction[]]): Transaction[] {
  if (fbUser === undefined) {
    return [];
  }
  return transactions.filter((transaction: Transaction) => {
    return transaction.participants.some((participant) => participant.debtor === fbUser.uid);
  });
}
