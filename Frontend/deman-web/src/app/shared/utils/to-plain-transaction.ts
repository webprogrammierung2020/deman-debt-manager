import { Transaction } from '@build/openapi';

export function toPlainTransaction(transactions: Transaction[]): Transaction[] {
  return transactions.filter((transaction: Transaction) => transaction.participants.length === 1);
}
