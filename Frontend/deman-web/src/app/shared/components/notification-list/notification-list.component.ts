import { Component } from '@angular/core';
import { NotificationService } from '@core/services/notification.service';
import { Observable } from 'rxjs';
import { Notification } from '@build/openapi';
import { map, take } from 'rxjs/operators';
import { toUnreadNotification } from '@shared/utils/to-unread-notifications';

@Component({
  selector: 'shared-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.scss'],
})
export class NotificationListComponent {
  unreadNotifications$: Observable<Notification[]> = this.notificationService.notifications$.pipe(
    map(toUnreadNotification)
  );
  loading$: Observable<boolean> = this.notificationService.loading$;

  constructor(private notificationService: NotificationService) {}

  onReloadNotifications(): void {
    this.notificationService
      .reloadNotifications()
      .pipe(take(1))
      .subscribe(() => console.log('success!'));
  }
}
