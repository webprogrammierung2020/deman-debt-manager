import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NotificationContent } from '@build/openapi';
import ExpectedAnswersEnum = NotificationContent.ExpectedAnswersEnum;

@Component({
  selector: 'shared-notification-actions',
  templateUrl: './notification-actions.component.html',
  styleUrls: ['./notification-actions.component.scss'],
})
export class NotificationActionsComponent {
  @Input() expectedAnswers: ExpectedAnswersEnum | undefined;
  @Input() loadingAgree = false;
  @Input() loadingDecline = false;
  @Input() loadingOk = false;
  // TODO this needs to be refactored to a separate type in openAPI yaml!!!!
  @Output() answerNotification: EventEmitter<'agree' | 'disagree' | 'ok'> = new EventEmitter<
    'agree' | 'disagree' | 'ok'
  >();
  AGREE_DISAGREE = ExpectedAnswersEnum.AgreeDisagree;
  OK = ExpectedAnswersEnum.Ok;

  constructor() {}

  onAgree(): void {
    this.answerNotification.next('agree');
  }

  onDisagree(): void {
    this.answerNotification.next('disagree');
  }

  onOk(): void {
    this.answerNotification.next('ok');
  }
}
