import { Component, Input, OnInit } from '@angular/core';
import {
  Notification,
  NotificationsService,
  SingleNotificationResponse,
  UserService,
} from '@build/openapi';
import { ReadableNotificationFactory } from '@core/services/readable-notification.factory';
import { ReadableNotification } from '@shared/interfaces/readable-notification';
import { filter, map, switchMap, take } from 'rxjs/operators';
import { NotificationEntityService } from '@core/store/ngrx-data/notifications/notification-entity.service';
import { AuthService } from '@core/services/auth.service';
import { notEmpty } from '@shared/utils/not-empty';
import firebase from 'firebase';
import { SnackbarService } from '@core/services/snackbar.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'shared-single-notification',
  templateUrl: './single-notification.component.html',
  styleUrls: ['./single-notification.component.scss'],
})
export class SingleNotificationComponent implements OnInit {
  @Input() notification: Notification;
  readableNotification: ReadableNotification | undefined;
  creator$: Observable<string>;
  description = '';

  loadingAgree = false;
  loadingDecline = false;
  loadingOk = false;

  constructor(
    private readableNotificationFactory: ReadableNotificationFactory,
    private userService: UserService,
    private notificationService: NotificationsService,
    private notificationEntityService: NotificationEntityService,
    private authService: AuthService,
    private snackBarService: SnackbarService
  ) {}

  ngOnInit(): void {
    console.log(this.notification);
    this.readableNotification = this.readableNotificationFactory.create(this.notification.content!);
    this.description = this.notification.content?.transactionDescription!;
    this.creator$ = this.userService
      .getUserByFirebaseUID(this.notification.sender!)
      .pipe(map((userResponse) => userResponse.body.user.email!));
  }

  onAnswerNotification(answer: 'agree' | 'disagree' | 'ok'): void {
    console.log(answer);
    this.setLoadingFlag(answer);
    this.notificationService
      .markNotificationAsRead(this.notification.notificationID!, answer)
      .pipe(take(1))
      .subscribe(
        (patchedNotficationResposne: SingleNotificationResponse) => {
          this.notificationEntityService.updateOneInCache(
            patchedNotficationResposne.body.notification
          );
        },
        (error) => {
          console.log('Something went wrong pathing the Notification!');
          console.log(error);
          // check if notification has been deleted
          this.snackBarService.openDefaultSnack(
            'Ein anderer Nutzer hat die Transaktion bereits abgelehnt! Wir laden deine Benachrichtigungen neu!',
            'Schließen',
            { duration: 10000 }
          );
          this.notificationEntityService.clearCache();
          this.authService.firebaseUser$
            .pipe(
              filter(notEmpty),
              take(1),
              switchMap((fbUser: firebase.User) => {
                return this.notificationService
                  .findNotificationsForUserById(fbUser.uid)
                  .pipe(map((notificationResponse) => notificationResponse.body.notification_list));
              }),
              take(1)
            )
            .subscribe(
              (notificationList) => {
                this.notificationEntityService.addManyToCache(notificationList);
              },
              (err) =>
                this.snackBarService.openDefaultSnack(
                  'Es scheint Probleme mit dem Server zu geben, bitte versuche es später erneut!!',
                  'Schließen',
                  { duration: 10000 }
                )
            );
        },
        () => this.resetLoadingFlags()
      );
  }

  private setLoadingFlag(answer: 'agree' | 'disagree' | 'ok'): void {
    if (answer === 'agree') {
      this.loadingAgree = true;
    } else if (answer === 'disagree') {
      this.loadingDecline = true;
    } else {
      this.loadingOk = true;
    }
  }

  private resetLoadingFlags(): void {
    this.loadingOk = false;
    this.loadingAgree = false;
    this.loadingDecline = false;
  }
}
