import { Component, OnInit } from '@angular/core';
import { claimListDataSet } from '../../interfaces/claimListDataSet';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss'],
})
export class DemoComponent implements OnInit {
  form: FormGroup;

  displayedColumns: string[] = ['transactionId', 'content', 'createdAt', 'sum'];
  dataSet: claimListDataSet[] = [
    {
      transactionId: 1,
      createdBy: 0,
      createdAt: '2021-02-19T15:17:24.099Z',
      content: 'Mittagessen',
      creditor: 0,
      sum: 12.5,
      debitor: 0,
      acknowlegded: 0,
    },
    {
      transactionId: 2,
      createdBy: 0,
      createdAt: '2021-02-19T15:17:24.099Z',
      content: 'Abendessen',
      creditor: 0,
      sum: 20,
      debitor: 0,
      acknowlegded: 0,
    },
    {
      transactionId: 3,
      createdBy: 0,
      createdAt: '2021-02-19T15:17:24.099Z',
      content: 'Frühstück',
      creditor: 0,
      sum: 10,
      debitor: 0,
      acknowlegded: 0,
    },
    {
      transactionId: 4,
      createdBy: 0,
      createdAt: '2021-02-19T15:17:24.099Z',
      content: 'Bar',
      creditor: 0,
      sum: 8.5,
      debitor: 0,
      acknowlegded: 0,
    },
    {
      transactionId: 5,
      createdBy: 0,
      createdAt: '2021-02-19T15:17:24.099Z',
      content: 'Bouldern',
      creditor: 0,
      sum: 11,
      debitor: 0,
      acknowlegded: 0,
    },
    {
      transactionId: 6,
      createdBy: 0,
      createdAt: '2021-02-19T15:17:24.099Z',
      content: 'Danke fürs Einkaufen',
      creditor: 0,
      sum: 50.6,
      debitor: 0,
      acknowlegded: 0,
    },
  ];

  constructor() {}

  ngOnInit(): void {
    this.form = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.email]),
      comment: new FormControl('', [Validators.required, Validators.minLength(4)]),
    });
  }
}
