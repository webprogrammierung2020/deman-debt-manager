import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AuthService } from '@core/services/auth.service';

@Component({
  selector: 'shared-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent {
  email: string;

  constructor(
    public dialogRef: MatDialogRef<ResetPasswordComponent>,
    private authService: AuthService
  ) {}

  onClose(): void {
    this.dialogRef.close();
  }

  onResetPassword(): void {
    this.authService
      .resetPassword(this.email)
      .then(() => {
        // TODO SUCCESS TOAST
        console.log('send mail!');
        this.dialogRef.close();
      })
      .catch((error) => console.log(error.message));
  }
}
