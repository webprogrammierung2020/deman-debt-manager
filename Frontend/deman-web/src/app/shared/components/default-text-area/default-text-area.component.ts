import { Component, Input } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'shared-default-text-area',
  templateUrl: './default-text-area.component.html',
  styleUrls: ['./default-text-area.component.scss'],
})
export class DefaultTextAreaComponent {
  @Input() parentForm: FormGroup;
  @Input() controlName: string;
  @Input() label: string;
  @Input() placeholder: string;
  @Input() classList = '';
  @Input() readonly = false;

  get control(): AbstractControl {
    // tslint:disable-next-line:no-non-null-assertion
    return this.parentForm.get(this.controlName)!;
  }
}
