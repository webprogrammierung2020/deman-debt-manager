import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ReadableTransactionTableItem } from '@shared/interfaces/readable-transaction-table-item';

@Component({
  selector: 'shared-claim-list',
  templateUrl: './claim-list.component.html',
  styleUrls: ['./claim-list.component.scss'],
})
export class ClaimListComponent implements OnInit, AfterViewInit {
  @Input() dataSet: ReadableTransactionTableItem[];
  @Input() displayedColumns: string[];
  @Input() type: string;

  @Output() clickedTransaction: EventEmitter<number> = new EventEmitter<number>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  dataSource: MatTableDataSource<ReadableTransactionTableItem>;

  constructor() {}

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<ReadableTransactionTableItem>(this.dataSet);
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: any): void {
    if (filterValue.target.value !== null) {
      this.dataSource.filter = filterValue.target.value.trim().toLowerCase();
    }
  }

  onClickRow(row: ReadableTransactionTableItem): void {
    this.clickedTransaction.emit(row.id);
  }
}
