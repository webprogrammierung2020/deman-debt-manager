import { Component } from '@angular/core';
import { MatSnackBarRef } from '@angular/material/snack-bar';
import { AuthService } from '@core/services/auth.service';

@Component({
  selector: 'shared-email-verification-hint',
  templateUrl: './email-verification-hint.component.html',
  styleUrls: ['./email-verification-hint.component.scss'],
})
export class EmailVerificationHintComponent {
  constructor(public snackBarRef: MatSnackBarRef<EmailVerificationHintComponent>) {}

  onResend(): void {
    // send callback to service
    this.snackBarRef.dismissWithAction();
  }
}
