import { Component, OnInit, Input } from '@angular/core';
import { ThemePalette } from '@angular/material/core';

/*
  TODO: Siehe Mockup buttons verschiedene Farben, Transparent Mit Outline, Ladeanimation (optional)
 */
@Component({
  selector: 'shared-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent implements OnInit {
  @Input() title: string;
  @Input() color: ThemePalette;
  @Input() buttonType = 'button';
  @Input() loading = false;
  @Input() ClassList = '';

  constructor() {}

  ngOnInit(): void {}
}
