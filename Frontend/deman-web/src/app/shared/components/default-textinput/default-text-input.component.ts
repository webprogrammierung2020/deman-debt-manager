import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'shared-default-text-input',
  templateUrl: './default-text-input.component.html',
  styleUrls: ['./default-text-input.component.scss'],
})
export class DefaultTextInputComponent {
  @Input() parentForm: FormGroup;
  @Input() controlName: string;
  @Input() label: string;
  @Input() placeholder: string;
  @Input() inputType = 'input';
  @Input() classList = '';
  @Input() readonly = false;

  get control(): AbstractControl {
    return this.parentForm.get(this.controlName)!;
  }

  constructor() {}
}
