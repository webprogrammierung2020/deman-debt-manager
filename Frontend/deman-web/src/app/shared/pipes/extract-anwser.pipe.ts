import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'extractAnswer',
})
export class ExtractAnswerPipe implements PipeTransform {
  transform(value: string): unknown {
    return value === 'agree' ? 'Akzeptiert' : 'Abgelehnt';
  }
}
