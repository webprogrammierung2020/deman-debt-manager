import { NavigationActionTiming, RouterState } from '@ngrx/router-store';

export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: 'AIzaSyCtw5jM-Z9Rjy4EPSHp6bO_utmtoEszxKw',
    authDomain: 'deman-52fe0.firebaseapp.com',
    projectId: 'deman-52fe0',
    storageBucket: 'deman-52fe0.appspot.com',
    messagingSenderId: '384146131391',
    appId: '1:384146131391:web:d551efc327a5457684390a',
  },
  storeDevtoolsConfig: {
    maxAge: 25,
    logOnly: true,
  },
  routerStoreConfig: {
    stateKey: 'router',
    routerState: RouterState.Minimal,
    navigationActionTiming: NavigationActionTiming.PostActivation,
  },
  xApiKey: 'A6BvubIrJw1C1JcJs9OJI3vy49u8V1yJvkek0aS0',
  personalRoutes: [
    {
      icon: 'dashboard',
      route: '/dashboard/home',
      title: 'Persöhnliches Dashboard',
    },
    {
      icon: 'add_circle',
      route: 'dashboard/create-claim',
      title: 'Transaktion hinzufügen',
    },
  ],
  groupRoutes: [
    {
      icon: 'groups',
      route: '',
      title: 'Gruppen',
    },
    {
      icon: 'group_add',
      route: '',
      title: 'Gruppe hinzufügen',
    },
  ],
  accountRoutesLoggedIn: [
    {
      icon: 'settings',
      route: 'settings',
      title: 'Account Einstellungen',
    },
  ],
  accountRoutesNotLoggedIn: [
    {
      icon: 'login',
      route: 'auth/login',
      title: 'Einloggen',
    },
    {
      icon: 'face',
      route: 'auth/signup',
      title: 'Registrieren',
    },
  ],
  plainTransactionNotificationBase: {
    create: {
      title: 'Erstellung einer Einzel Transaktion',
      body: 'Ein Nutzer hat beantragt eine Einzeltransaktion zu erstellen.',
      isResponse: false,
    },
    response: {
      title: 'Antwort zur Erstellung einer Einzeltransaktion',
      body: 'Die zu erstellende Einzeltransaktion wurde vom angefragten Nuttzer beantwortet!',
      isResponse: true,
    },
  },
  bucketTransactionNotificationBase: {
    create: {
      title: 'Erstellung einer Sammel Transaktion',
      body:
        'Ein Nutzer hat beantragt eine Sammeltransaktion zu erstellen, an der sie einen Anteil tragen sollen.',
      isResponse: false,
    },
    response: {
      title: 'Antwort zur Erstellung einer Sammeltransaktion',
      body: 'Die beteiligten der von ihnen erstellten Sammeltransaktionen haben geantwortet. ',
      isResponse: true,
    },
  },
  groupTransactionNotificationBase: {
    create: {
      title: 'Erstellung einer Gruppen Transaktion',
      body: 'In einer ihrer Gruppen wurde eine neue Transaktion erstellt. ',
      isResponse: false,
    },
    response: {
      title: 'Antwort zur erstellung einer Gruppentransaktion',
      body:
        'Die angefragte Gruppe hat eine Antwort zur Erstellung der Gruppentransaktion von ihnen gegeben.',
      isResponse: true,
    },
  },
  compensationNotificationBase: {
    create: {
      title: 'Begleichung einer Schuld',
      body: 'Es wurde eine Begleichungszahlung ihnen gegenüber erstellt.',
      isResponse: false,
    },
    response: {
      title: 'Antwort zur Begleichung einer Schuld',
      body:
        'Die angefragte Begleichungstransaktion wurde von den verantwortlichen Beteilligten bearbeitet.',
      isResponse: true,
    },
  },
};
